    The support program bf(icm-un) is called by icm() tt(-u), expecting one
argument, a bim-file. It disassembles the binary file an shows the assembler
instructions and the structure of the bim-file.

As an illustration, assume the following script is compiled 
by tt(icmake -c demo.im)):
        verb(
    void main()
    {
        printf << "hello world\n";
    }
        )
    the resulting tt(demo.bim) file is disasembled by icm() tt(-u demo.bim)
writing the following to the standard output fle:
 verb(
    icm-un by Frank B. Brokken (f.b.brokken@rug.nl)
    icm-un V12.04.00
    Copyright (c) GPL 1992-2024. NO WARRANTY.
    
    Binary file statistics:
        strings      at offset  0x0025
        variables    at offset  0x0033
        filename     at offset  0x0033
        code         at offset  0x0014
        first opcode at offset  0x0021
    
    String constants dump:
        [0025 (0000)] ""
        [0026 (0001)] "hello world."
    
    Disassembled code:
        [0014] 06 01 00   push string "hello world."
        [0017] 05 01 00   push int 0001
        [001a] 1b 1d      callrss 1d (printf)
        [001c] 1c 02      add sp, 02
        [001e] 04         push int 0
        [001f] 24         pop reg
        [0020] 23         ret
        [0021] 21 14 00   call [0014]
        [0024] 1d         exit
)

Offsets are shown using the hexadecimal number system and are absolute byte
offsets in the bim-file. The string constants dump also shows, between
parentheses, the offsets of the individual strings relative to the beginning
of the strings section. The output also shows the opcodes of the instructions
of the compiled tt(.im) source files. If opcodes use arguments then these
argument values are shown following their opcodes. Each opcode line ends by
showing the opcode's mnemonic plus (if applicable) the nature of its argument.
