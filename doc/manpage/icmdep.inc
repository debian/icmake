
bf(Icm-dep) is a support program called by icm() tt(--dependencies) (or its
short alternative tt(-d)) to determine source- and precompiled-header file
dependencies. 

When using bf(icmbuild)(1) and an tt(icmconf) file contains the tt(#define
SPCH) directive icm() tt(-d) is not used; instead, directories inspected when
using a SPCH are automatically inspected (and updated when necessary) by
icm() tt(-S).

Icm() tt(-d)'s synopsis is
        verb(    icmake -d [options] argument)
    Specifications following tt(-d) are forwared to bf(icm-dep). The following
options can be specified after tt(-d):
    itemization(
    it() lsvoption(classes)(c)(filename)nl()
       By default, bf(icm-dep) inspects dependencies of the directories
        mentioned in the file tt(CLASSES). If the bf(icmconf)(7)
        file specifies tt(PARSER_DIR) and/or tt(SCANNER_DIR) then those
        directories are also considered.  Use this option to specify the file
        containing the names of directories to be inspected by bf(icm-dep).

    it() loption(gch)nl() 
       If an tt(icmconf) file specifies the (deprecated) tt(#define PRECOMP)
        directive then icm() tt(-d) checks whether precompiled headers must be
        refreshed.  If an tt(icmconf) file does not contain a tt(#define
        PRECOMP) diretive, but precompiled headers should nonetheless be
        inspected, then option tt(--gch) can be specified;

    it() lsoption(help)(h)nl()
       A summary of bf(icm-dep)'s usage is written to the standard output and
        icm() terminates, returning 0 to the operating system;

    it() lsvoption(icmconf)(i)(filename)nl() 
       By default icm() tt(-d) inspects the content of tt(icmconf) files, This
        option is used if instead of tt(icmconf) another file should be
        inspected;

    it() lsvoption(mainih)(m)(mainheader)nl()
       In the tt(icmconf) file the tt(#define IH) directive is used to specify
        the suffix of class header files that should be precompiled, assuming
        that their filenames are equal to the names of the directories which
        are listed in the tt(CLASSES) file. But tt(CLASSES) does not specify
        the name of the program's top-level directory. This option is used to
        specify the name of the top-level header file to precompile. By
        default tt(main.ih) is used;

    it() loption(no-gch)nl() 
       If an tt(icmconf) file contains a (deprecated) tt(#define PRECOMP)
        directive but icm() tt(-d) should not check whether precompiled
        headers must be refreshed then specify option tt(--no-gch);

    it() loption(no-use-all)nl()
       If this option is specified then the tt(#define USE_ALL "filename")
        directive in the tt(icmconf) file (cf. bf(icmconf)(7)) is ignored;

    it() lvoption(use-all)(filename)nl()
       If this option is specified then all files in directories containing
        the file tt(filename) and source files in directories (recursively)
        depending on the files in those directories are recompiled;

    it() lsoption(verbose)(V)nl() 
       This option can be specified multiple times. The number of times it is
        specified determines the verbosity of icm() tt(-d)'s output. If not
        specified then icm() tt(-d) silently performs its duties. If specified
        once (which is the default specification), then icm() tt(-d) writes to
        the standard output the actions it performs; if specified twice it
        also reports non-default options and automatically included
        directories; if specified three times it also reports class
        dependencies; if specified more often it reports what files it
        encountered and what decision it would make when tt(go) would be
        specified;

    it() lsoption(version)(v)nl() 
       Icm() tt(-d) writes bf(icm-dep)'s version number to the standard
        output and terminates, returning 0 to the operating system.
    )

Following the non-terminating options argument tt(go) must be specified to
allow icm() tt(-d) to perform its actions. Specifying another argument results
in a `dry run': it analyzes dependencies, but won't remove or touch files.

Icm() tt(-d) can be used for software projects which are developed as
described in the
    url(C++ Annotations)(https://fbb-git.gitlab.io/cppannotations/), section
tt(Header file organization) in chapter tt(Classes). For those projects
classes are developed in their own directories, which are direct
sub-directories of the project's main program directory. Their class
interfaces are provided in class-header files bearing the names of the
class-directories, and all headers that are required by the class's sources
are declared in a separate em(internal header) files, commonly having
extensions tt(.ih).
