Icm() supports multi-threaded source-file compilation, often significantly
reducing the compilation time of the source files of projects. When using
bf(icmbuild)(1) multi-threaded compilation is automatically used when
tt(icmconf) files contain the tt(#define MULTICOMP) directive
(cf. bf(icmconf)(7)). It can also be called independently from tt(icmconf)
using icm()'s tt(--multicomp) (or tt(-m)) option.

Icm() tt(-m)'s synopsis is
        verb(    icmake -m [options] arguments)

Icm() tt(-m) accepts the following options:
    itemization(
    it() lsoption(help)(h)nl()
       Icm() tt(-m) writes a summary of its usage to the standard output
        and terminates, returning 0 to the operating system;

    it() lsoption(nr)(n)nl()
       When compiling source files and option tt(--nr) is specified then the
        thread number compiling a source file is written to the standard
        output stream.

    it() lsoption(quiet)(q)nl()
       When this options is not specified then the path names of the
        compiled object and source files are written to the standard output
        stream. When it is specified once only the source files' directories
        and filenames are written to the standard output stream, and when it
        is specified more than once no information about the compiled files is
        written to the standard output stream.

    it() lsvoption(threads)(t)(nThreads)nl()
       By default the computer's number of cores determines the number of
        threads being used when compiling the source files. A different number
        of threads can be requested using this option, e.g., tt(--threads 5).

    it() lsoption(version)(v)nl() 
       Icm() tt(-m) reports its version number to the standard output and
        terminates, returning 0 to the operating system.
    )

Icm() tt(-m) needs one command-line argument and an optional second
argument: 
    itemization(
    it() the first argument is the name of the file specifying which files
        must be compiled. Use bf(icmbuild)(1) to write this file. It can also
        be constructed otherwise:nl()
       The specified file must contain groups of file specifications where
        each group starts with a line like tt(: support tmp/o 5) where the 2nd
        element (here: tt(support)) specifies the (sub-)directory of the
        source files (use tt(.) to refer to the project's top-level
        directory); the 3rd element (here: tt(tmp/o)) specifies the
        destination directory of the compiled files (which is created if not
        existing); and the 4th element (here: tt(t)) specifies the prefix to
        add in front of the compiled object files.nl()
       Following this line the remaining lines of a group specify the names of
        the source files (located in specified (sub-)directory) to compile.nl()
       Once the compilation ends (either because all files were successfully
        ccompiled, or because a compilation failed) the specification file is
        removed;
    it() the second argument is optional. By default the following
        specification is used  (all on one line)
       verb(
    g++ -c -o $2 ${ICMAKE_CPPSTD} --Wall -Werror $1
       )
       Here tt($1) is replaced by the location of the source file to compile
        and tt($2) is replaced by the location of the compiled object file. If
        the environment variable tt(ICMAKE_CPPSTD) is defined (specifying the
        bf(C++) standard to use, e.g., tt(ICMAKE_CPPSTD=--std=c++26)) then its
        value replaces tt(${ICMAKE_CPPSTD}) in the specification.nl()
       If the default compiler specification cannot be used the command to
        compile source files can be provided as icm() tt(-m)'s second
        command-line argument, which should be quoted, like
       verb(
    'g++ -c -o $2 '${ICMAKE_CPPSTD}' --Wall -Werror $1'
       ) 
       or the second command-line argument can be tt(f:file), where tt(file)
        is the name of a file whose first line contains the specification of
        the command compiling source files (which must specify tt($1) and
        tt($2) and optionally tt($ICMAKE_CPPSTD)).nl()
       The tt(PATH) environment variable is used to locate
        the compiler; the compiler's absolute path can also be specified.
    )
