Single Pre-Compiled Headers (SPCH) are available using icm() tt(--spch)
(or tt(-S)). 

Icm() tt(-S)'s synopsis is
        verb(    icmake -S [options] arguments)

Icm() tt(-S) accepts the following options:
    itemization(
    it() lsvoption(all)(a)(file)nl()
       Call in sequence bf(icm) tt(-S)'s options tt(--list, --precompile), and
        tt(--soft-links) (see below), where tt(file) is written by option
        tt(--list) and used as option argument for tt(--precompile) and
        tt(--soft-links);

    it() lsvoption(classes)(c)(filename)nl()
       The file tt(filename) is the name of a file containing a list of
        directories inspected by the tt(--list) option (by default
        CLASSES). The project's top directory is automatically inspected
        unless the option tt(--no-topdir) is specified;

    it() lsoption(help)(h)nl()
       Icm() tt(-S) writes a summary of its usage to the standard output
        and terminates, returning 0 to the operating system;

    it() lsvoption(internal)(i)(.ext)nl() 
        tt(.ext) is the extension used for the internal headers (including the
        dot). By default tt(.ih) is used;

    it() lsoption(list)(l)nl()
       Ignored when option tt(--all) is specified.nl()
       Write the names of the files to process when constructing an SPCH
        to the file specified as icm() tt(-S)'s first command line
        argument. The specified filename may not have an extension or
        directory specifications (e.g., tt(spch));

    it() lsoption(no-topdir)(n)nl()
        Ignore the internal header found in the project's top directory. This
        option is used when, e.g., merely constructing a library instead of a
        program;

    it() lsvoption(precompile)(p)(file)nl() 
       Precompile tt(file) (the name of the file specified at the option
        tt(--list)) to the SPCH file specified as icm() tt(-S)'s first
        command-line argument. If that argument ends in tt(/) then the SPCH
        tt('argument'file.gch) is written.nl()
       By default the SPCH is constructed by the following command (all on
        one line):
       verb(
    g++ -c -o $2 ${ICMAKE_CPPSTD} -Wall -Werror -O2 -x c++-header $1
       )
       Here, $1 is replaced by 'file', and $2 is replaced by the name of the
        SPCH, while tt($ICMAKE_CPPSTD) refers to the value of the
        tt(ICMAKE_CPPSTD) environment variable (specifying the bf(C++)
        standard to use, e.g., ICMAKE_CPPSTD=--std=c++26).nl()
       Alternatively, the command constructing the SPCH can be provided as
        second command-line argument, which should be quoted like
       verb(
    'g++ -c -o $2 '${ICMAKE_CPPSTD}' -Wall -Werror -O2 -x c++-header $1'
       )
       or the second command-line argument can be tt(f:file), where tt(file)
        is the name of a file whose first line specifies the command
        constructing the SPCH (which must specify tt($1) and tt($2) and
        optionally tt($ICMAKE_CPPSTD)).nl()
       The tt(PATH) environment variable is used to locate
        the compiler; the compiler's absolute path can also be used.

    it() lsoption(quiet)(q)nl()
       By default the (tt(g++)) command constructing the single precompiled
        header file is echoed to the standard output stream. Specify this
        option to suppress writing the command to the standard output stream.

    it() lsvoption(soft-links)(s)(file)nl() 
       Ignored when option tt(--all) is specified.nl()
       This option uses the same arguments as used with the tt(--precompile)
        option. This option creates tt(.gch) soft-links from the header files
        listed in tt(file) to the generated SPCH-file;

    it() lsvoption(unused)(u)(regex)nl() 
       In practice this option is probably not required, but files matching
        (POSIX extended) tt(regex) are not inspected by icm() tt(-S). When
        used specifications like tt((...)|(...)) can be used to specify
        multiple regexes. Alternatively tt(f:file) can be used as option
        argument to specify a file whose non-empty lines contain regexes;

    it() lsoption(version)(v)nl() 
       Icm() tt(-S) writes its version number to the standard output and
        terminates, returning 0 to the operating system;

    it() lsoption(warn)(w)nl() 
       interactively warn when existing header files are about to be modified,
        accepting or refusing the modifications. Once refused bf(icm-spch)
        ends. 
    )

COMMENT(
    it() lsvoption(required)(r)(.ext)nl() 
       this option can only be used in combination with the tt(--list) option
        and not when option tt(--include) is specified. 
       tt(.ext) is the extension used for the `required' headers (including
        the dot). This option is currently only used for developmental
        purposes and should in practice be avoided;
END)

Icm() tt(-S) needs one, and optionally two arguments, which were described at
the tt(--list, --precompile), and tt(soft-links) option descriptions.


Pre-compiled headers have been available for quite some time, and usually
result in a significant reduction of the compilation time. Using single
precompiled headers results in a large reduction of required disk-space
compared to using precompiled headers for separate directories.

When using SPCHs almost identical precompiled headers for separate directories
are avoided: only one precompiled header is constructed which is then used by
all components of a project. As identical sections are avoided the sizes (and
construction times) of SPCHs are much smaller, usually requiring only 5 to 10 %
of the space (and construction time) ccompared to using separately constructed
pre-compiled headers.

SPCHs can easily be used in combination with bf(icmbuild)(1). Often a
specification in a project's tt(icmconf) file like tt(#define SPCH "") is all
it takes (cf. bf(icmconf)(7)).






