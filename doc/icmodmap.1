.TH "icmodmap" "1" "1992\-2025" "icmake\&.13\&.02\&.00" "initializes C++ projects using modules"

.PP 
.SH "NAME"
icmodmap \- Initializes the maintenance of C++ projects using
modules
.PP 
.SH "SYNOPSIS"
\fBicmodmap\fP \fI[Options] [arg]\fP
.PP 
.SH "DESCRIPTION"

.PP 
Although modules have already been available for quite some time, the Gnu
\fIg++\fP compiler still shows a number of bugs related to C++ modules
(cf\&. \fIhttps://gcc\&.gnu\&.org/bugzilla/show_bug\&.cgi?id=103524\fP)\&. It\(cq\&s possible
that you stumble across some of these (or new) bugs when using modules\&. The
number of reported bugs is gradually reducing, and the current state of
affairs at least allows you to gain some experience in using modules\&.
.PP 
Modules cannot mutually import each other\&. Consequently there\(cq\&s always a
linear, non\-circular module hierarchy, which should be acknowledged when
compiling files defining and using modules\&. \fBIcmodmap\fP determines this order and
prepares the construction of programs using modules\&. Once \fBicmodmap\fP has done its
job your favorite build\-utility can be used as before\&. Just make sure that
compilation uses the same \fBC++\fP standard as used by \fBicmodmap\fP, and provide the
\fI\-fmodules\-ts\fP compiler option when compiling source files\&. 
.PP 
\fBIcmodmap\fP assumes that modules are defined in separate sub\-directories\&. For your
own documentation it\(cq\&s advised that filenames containing module (and
partition) interface units start with \fImod\fP (e\&.g\&., \fImodsupport\&.cc\fP\&. If a
program defines two modules \fIFirst\fP and \fISecond\fP (e\&.g\&., in sub\-directories
\fIfirst\fP and \fIsecond\fP and \fIsecond/modsecond\&.cc\fP imports module \fIFirst\fP
then \fIfirst/modfirst\&.cc\fP must be compiled before compiling
\fIsecond/modsecond\&.cc\fP\&. This requirement is handled by \fBicmodmap\fP\&. Once the source
files containing module interface units have been compiled the program\(cq\&s
remaining source files can be compiled as usual\&.
.PP 
By default \fBicmodmap\fP expect projects using modules to be organized this way:
.IP o 
The project\(cq\&s top\-level directory contains a file \fICLASSES\fP, where
each line (ignoring (\fBC++\fP) comment) specifies the name of a
sub\-directory implementing one of the project\(cq\&s components\&. Each
sub\-directory can (but does not have to) define a module, a module
partition, a class (not part of a module, but maybe using modules), or
other (source) files, maybe using modules;
.IP o 
If the project defines a program, the project\(cq\&s top\-level directory
contains source files (like \fImain\&.cc\fP) defining the \fImain\fP
function\&. \fBIcmodmap\fP also recognizes  module interface
units defined in the top\-level directory (but, for clarity, module
interface units should probably be defined in their own
sub\-directories)\&.
.IP o 
Since partitions define sub\-components of modules, consider defining
partitions in sub\-directories of their module asub\-directories\&. When
adopting this organization then these sub\-sub\-directories are
specified in the \fICLASSES\fP file\&. E\&.g\&.,
.nf 

    module
    module/part1
    module/part2
 
.fi 
.IP o 
Modules and partitions may define classes (preferably one class per
module, one class per partition)\&. If so the member functions of those
classes can be defined in the module or partition\(cq\&s directory\&.

.PP 
\fBIcmodmap\fP inspects each directory specified in the \fICLASSES\fP file, including
the top\-level directory\&. It recognizes interface units and source files
implementing module components\&. It ignores comment, but the individual parts
of specifications can be separated by spaces or tabs but not by comment\&. E\&.g\&.,
\fIexport /* */ module Support;\fP is not recognized\&.
.PP 
When compiling source files containing interface units the compiler is 
called using at least the following options (see also the options \fI\-\-colors,
\-\-extension\fP and \fI\-\-subdir\fP in section \fBOptions\fP):
.nf 
    /bin/g++ ${ICMAKE_CPPSTD} \-fmodules\-ts \-c \-Wall
.fi 
If the environment variable ttICMAKE_CPP) standard is not defined then the
\fBC++\fP standard used by default by the compiler is used\&. To define it use
e\&.g\&., \fIexport ICMAKE_CPPSTD=\-\-std=c++26\fP\&.
.PP 
.SH "The program\(cq\&s argument"

.PP 
The program\(cq\&s argument can be omitted or it can be specified in various ways:
.IP o 
when not specified \fBicmodmap\fP starts its mapping process, according to
its default or explicitly specified options\&. E\&.g, when merely calling
\fBicmodmap\fP it locates the project\(cq\&s source files containing interface
units, determines their compilation order, and compiles those files in
the determined order;
.IP o 
when specified as \fIclean\fP the project\(cq\&s \fIgcm\&.cache\fP sub\-directory
and the \fIgcm\&.cache\fP soft\-links are removed\&. Compiled object files
(e\&.g\&., files under \fItmp/o\fP are not removed; removing those files is
left to the used build\-utility);
.IP o 
when specified as \- interface unit files are not compiled but their
source file names are written in the required compilation order to the
standard output stream
.IP o 
otherwise \fBicmodmap\fP\(cq\&s argument is a filename to receive the names of the
source files that would have been written to the standard
output stream when argument \- would have been specified\&.

.PP 
.SH "OPTIONS"

.PP 
\fIIcmodmap\fP supports the following options:
.IP o 
\fB\-\-colors\fP (\fB\-c\fP)
.br 
by default the compiler is called using the option
\fI\-fdiagnostics\-color=never\fP\&. When this option is specified the
compiler messages may use colors;
.IP 
.IP o 
\fB\-\-dependencies\fP (\fB\-d\fP)
.br 
the dependencies between modules is shown using this option\&. Module
defining source files are not compiled\&. \fBIcmodmap\(cq\&s\fP output may look
like this:
.nf 
    Dependencies:
       LOCAL module Square
       LOCAL module DepSquare
           imports LOCAL module Square
 
.fi 
indicating that modules \fISquare\fP and \fIDepSquare\fP are defined by
the current project, and the module \fIDepSquare\fP depends on the
module \fISquare\fP\&.
.IP 
It\(cq\&s also possible that modules depend on modules defined by other
projects\&. In that case the output may look like this:
.nf 
    Dependencies:
       LOCAL module Square
           imports EXTERN module RectAngle
       LOCAL module DepSquare
           imports LOCAL module Square
       EXTERN module RectAngle
 
.fi 
In this example module \fIRectAngle\fP is a module made available by some
other project\&. The local modue \fIDepSquare\fP, as before, depends on
\fISquare\fP\&. In this example the \fIRectAngle\&.gcm\fP file must be
available in the project\(cq\&s \fIgcm\&.cache\fP directory before module
\fISquare\fP can be compiled (it\(cq\&s normally made available via a
soft\-link to the actual file);
.IP 
.IP o 
\fB\-\-extension\fP=\fIext\fP (\fB\-x\fP)
.br 
\fIext\fP is the extension of the source files inspected by \fBicmodmap\fP\&. By
default it is \fI\&.cc\fP;
.IP 
.IP o 
\fB\-\-extern\fP=\fIarg\fP (\fB\-e\fP)
.br 
when modules depend on externally available modules then the locations
of the directories containing those \fI\&.gcm\fP files can be specified
using this option\&. If those external \fI\&.gcm\fP files are all located in
a single directory then the location of the that directory (absolute
or relative to the current project\(cq\&s top\-level directory) can be
specified by \fIarg\fP\&.
.IP 
If those external \fI\&.gcm\fP files are located in multiple directories
then the locations of those directories are specified in a file which
is specified by \fIarg\fP\&. In this case each of \fIarg\(cq\&s\fP lines
specifies the location of an external directory, ignoring  empty
lines and lines starting with \fI//\fP;
.IP 
.IP o 
\fB\-\-help\fP (\fB\-h\fP)
.br 
Provides usage info, returning 0 to the operating system;
.IP 
.IP o 
\fB\-\-ignore\fP=\fIlist\fP (\fB\-i\fP)
.br 
use this option instead of using the \fICLASSES\fP file\&. When using this
option all (one\-level deep) sub\-directories (and the project\(cq\&s
top\-level directory) are inspected except for those listed in the
\fIlist\fP argument\&. If \fIlist\fP must specify multiple directories then
surround the space\-separated sub\-directory names by quotes\&. To visit
all sub\-directories use \fI\(cq\&\(cq\&\fP;
.IP 
.IP o 
\fB\-\-library\fP=\fIlib\fP (\fB\-l\fP)
.br 
\fIlib\fP specifies the path (relative to the project\(cq\&s top\-level
directory) of a (library) file containing the project\(cq\&s \&.o files\&. To
determine whether source files defining interface units must be
(re)compiled their timestamps are not only compared to the timestamps
of their object files but also to the timestamp of the library;
.IP 
.IP o 
\fB\-\-mark\fP=\fIarg\fP (\fB\-m\fP)
.br 
\fIarg\fP can be +, ++, or a filename\&. When this option is used interface
units depending on other modified interface units are also compiled\&.
.IP 
If \fIarg\fP is ++ then the last write times of \fIall\fP files
implementing or using those interface units are set to the current
time\&.  
.IP 
If \fIarg\fP is neither + nor ++ then \fIarg\fP is the name of a file it
receiving the names of the files otherwise processed by \(cq\&++\(cq\&;
.IP 
.IP o 
\fB\-\-quiet\fP=\fIwhat\fP (\fB\-q\fP)
.br 
by default the compiler calls and the dependencies are written to the
std\&. output stream\&. Specify \(cq\&what\(cq\& as:
.br 
c \- to suppress showing the compiler commands;
.br 
d \- to suppress showing the dependencies;
.br 
any other argument: both \(cq\&c\(cq\& and \(cq\&d\(cq\&\&.
.br 
\fI\-\-verbose\fP suppresses \-\-quiet;
.IP 
.IP o 
\fB\-\-subdir\fP (\fB\-s\fP)
.br 
By default the project\(cq\&s \fItmp/o\fP sub\-directory receives the compiled
interface unit files, using their \fICLASSES\(cq\&\fP compilation order
number prefix (e\&.g\&., \fItmp/o/1modsquare\&.o, tmp/o/2modepquare\&.o,
\&.\&.\&.\fP)\&. When this options is specified the compiled files are located
in the same sub\-directory as their source files (in which case an
order number prefix is not used);
.IP 
.IP o 
\fB\-\-version\fP (\fB\-v\fP)
.br 
Displays \fBicmodmap\fP\(cq\&s version\&.
.IP 
.IP o 
\fB\-\-verbose\fP (\fB\-V\fP)
.br 
Shows additional output when determining the module compilation order
and compiling the module definition files\&.

.PP 
.SH "EXAMPLE"

.PP 
The following program defines a module \fISquare\fP in its sub\-directory
\fIsquare\fP, containing this \fImodsquare\&.cc\fP file:
.PP 
.nf 
export module Square;

export
{
    double square(double value);

    class Square
    {
        double d_amount;

        public:
            Square(double amount = 0);          // initialize
            void amount(double value);          // change d_amount
            double amount() const;              // return d_amount

            double lastSquared() const;         // returns g_squared
            double square() const;              // returns sqr(d_amount)
    };
}

double g_squared;

.fi 

.PP 
The \fImain\fP function imports the \fISquare\fP module and uses its
facilities\&. It also imports the \fIiostream\fP module\-compiled system
header\&. That header is not inspected by \fBicmodmap\fP, and must be available before
the \fImain\&.cc\fP file can be compiled\&. Here is the \fImain\&.cc\fP source file:
.PP 
.nf 
import Square;
import <iostream>;

int main(int argc, char **argv)
{
    std::cout << \(dq\&the square of \(dq\& << argc << \(dq\& is \(dq\& 
                                        << square(argc) << \(cq\&\en\(cq\&;
    Square obj{12};
    std::cout << \(dq\&the square of 12 is \(dq\& << obj\&.square() << \(dq\&\en\(dq\&
                \(dq\&the last computed square is \(dq\& << 
                obj\&.lastSquared() << \(cq\&\en\(cq\&;

}

.fi 

.PP 
Executing \fIicmodmap \-d\fP shows \fISquare\fP as a local module:
.nf 
    Dependencies:
       LOCAL module Square
 
.fi 

.PP 
Executing \fIicmodmap \-V\fP performs and shows the compilation of
\fIsquare/module\&.cc\fP as well as the names of the source files implementing
(declaring) its components or merely importing the module:
.nf 

    Defining a soft\-link from gcm\&.cache/usr \-> /usr
    Inspecting square/
       scanning square1\&.cc
           declares module Square
       scanning amount1\&.cc
           declares module Square
       scanning square\&.cc
           declares module Square
       scanning square2\&.cc
           declares module Square
       scanning modsquare\&.cc
           defines module Square
       scanning amount2\&.cc
           declares module Square
       scanning lastsquared\&.cc
           declares module Square
    Inspecting \&./
       scanning main\&.cc
           imports module Square
    Compiling\&.\&.\&.
       in square: /bin/g++ \-\-std=c++26 \-fdiagnostics\-color=never  \-c
            \-fmodules\-ts \-Wall \-o \&.\&./tmp/o/1modsquare\&.o modsquare\&.cc 
 
.fi 

.PP 
.SH "SEE ALSO"
\fBicmake\fP(1)\&.
.PP 
.SH "BUGS"
None reported\&.
.PP 
.SH "COPYRIGHT"
This is free software, distributed under the terms of the 
GNU General Public License (GPL)\&.
.PP 
.SH "AUTHOR"
Frank B\&. Brokken (\fBf\&.b\&.brokken@rug\&.nl\fP)\&.
.PP 
