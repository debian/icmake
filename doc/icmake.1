.TH "icmake" "1" "1992\-2025" "icmake\&.13\&.02\&.00" "A program maintenance utility"

.PP 
.SH "NAME"
icmake \- A program maintenance (\fImake\fP) utility using a
\fBC\fP\-like grammar
.PP 
.SH "SYNOPSIS"
\fBicmake\fP option(s) \fI[source [dest]]\fP [args]
.PP 
.SH "DESCRIPTION"

.PP 
\fBIcmake\fP(1) is a generic tool for program maintenance which can be
used as an alternative for, e\&.g\&., \fBmake\fP(1)\&. It\(cq\&s a generic tool in that
\fIicmake\fP\-scripts, written in a language closely resembling the \fBC\fP
programming language, can perform tasks that are traditionally the domain of
scripting languages\&. 
.PP 
This man\-page consists of the following sections:
.IP o 
\fBICMAKE V\&. 13\&.00\&.00\fP covers the changes since \fBicmake\fP version
12\&.03\&.00;
.IP o 
\fBOPTIONS\fP covers \fBicmake\fP\(cq\&s options;
.IP o 
\fBEXECUTING ICMAKE SCRIPTS\fP covers how to call \fBicmake\fP scripts;
.IP o 
\fBICM\-DEP\fP describes the \fIicm\-dep\fP support program;
.IP o 
\fBICM\-MULTICOMP\fP describes the \fIicm\-multicomp\fP support program;
.IP o 
\fBICM\-SPCH\fP describes the \fIicm\-spch\fP support program;
.IP o 
\fBICM\-UN\fP describes the \fIicm\-un\fP support program;
.IP o 
\fBFILES\fP provides an overvieuw of the locations of \fBicmake\fP and its
support programs (as used by the Debian Linux distribution);
.IP o 
\fBEXAMPLES\fP contains references to locations containing some
examples;
.IP o 
\fBSEE ALSO\fP contains references to man\-pages of \fBicmake\fP support
programs and man\-pages of programs related to \fBicmake\fP\&. In particular
.RS 
.IP o 
\fBicmstart\fP(1), describes how to initialize a directory for
(\fBC++\fP or \fBC\fP) program development;
.IP o 
\fBicmbuild\fP(1), covers how to use \fBicmake\fP for the maintenance
of such programs\&.
.IP o 
\fBicmscript\fP(7) covers the syntax and facilities of \fBicmake\fP\(cq\&s
scripting language, allowing you to write your own \fBicmake\fP
scripts\&.
.RE

.PP 
\fBIcmake\fP allows programmers to use a scripting language (closely resembling the
well\-known \fBC\fP\-programming language) to define the actions that are required
for (complex) program maintenance\&. For this, \fBicmake\fP offers various special
operators as well as a set of support functions that have shown their
usefulness in program maintenance\&.
.PP 
\fBIcmake\fP should not be confused with an Integrated Development Environment
(IDE)\&. \fBIcmake\fP merely performs tasks for which scripts can be written, and a
minimal set of pre\-defined scripts (\fBicmstart\fP and \fBicmbuild\fP) that have
proven their usefulness when developing and maintaining programs are included
in \fBicmake\(cq\&s\fP distribution\&.
.PP 
When using \fBicmbuild\fP(1) for program maintenance the following \fBicmake\fP support
programs are used: 
.IP o 
\fIicm\-comp\fP       byte\-code compiling an \fBicmake\fP script;
.IP o 
\fIicm\-dep\fP        handling class\-dependencies;
.IP o 
\fIicm\-exec\fP       executing a byte\-code compiled \fBicmake\fP script;
.IP o 
\fIicm\-multicomp\fP  (optionally) using multi\-threaded source file
compilation;
.IP o 
\fIicm\-pp\fP         pre\-processing an \fBicmake\fP script;
.IP o 
\fIicm\-spch\fP       (optionally) constructing a project\-wide Single
Pre\-Compiled Hbeader file (SPCH)\&.

.PP 
In addition to the above programs the \fBicmake\fP project provides
.IP o 
the \fBicmodmap\fP program can be used when developing \fIC++\fP
programs using modules (cf\&. the \fBicmodmap\fP(1) man\-page);
.IP o 
\fIicm\-un\fP disassembling compiled \fBicmake\fP byte\-code files
(`\fIbim\-files\fP\(cq\&)\&. \fBicm\-un\fP is primarily used for illustration,
education, and debugging and is called by \fBicmake\fP when specifying its
option \fI\-\-unassemble\fP or \fI\-u\fP (see also section \fBICMUN\fP)\&.

.PP 
Some make\-utilities by default recompile sources once header files are
modified\&. When developing \fBC++\fP programs this is often not required, as
adding new member functions to classes does not require the recompilatopm of
all source files of those classes\&. Class dependencies are optionally inspected
by \fBicmbuld\fP(1) (they are inspected when the \fIUSE_ALL, SPCH\fP, and/or 
(now deprecated) \fIPRECOMP\fP \fI#define\fP directives in the \fIicmconf\fP file
are activated (cf\&. the \fBicmconf\fP(7) man\-page for details)\&.
.PP 
.SH "ICMAKE V\&. 13\&.00\&.00"

.PP 
In \fBicmake\fP version 13\&.00\&.00 the following modifications were implemented
w\&.r\&.t\&. its version 12\&.03\&.00:
.IP o 
option \fI\-\-all\fP (\fI\-a\fP) was added to the \fBicm\-spch\fP program
performing in sequence the actions of its \fI\-\-list, \-\-precompile,\fP
and \fI\-\-soft\-link\fP options\&. Several existing options were
altered\&. See the \fBICM\-SPH\fP section for details\&.
.IP o 
the (internal) headers inspected by \fIicmake \-\-spch\fP (or \fI\-S\fP) are
not modified anymore\&. 
.IP o 
to remove the (now obsoleted) \fI#ifndef SPCH_\fP specifications from
files listed in existing \fIspch\fP files written by previous versions
of \fBicm\-spch\fP the program \fInoifndef\&.cc\fP, available in the \fBicmake\fP
source distribution in its \fIsupport/\fP sub\-directory can be used to
remove existing \fI#ifnef SPCH_\fP sections from a project\(cq\&s \fI\&.ih\fP
files;
.IP o 
\fBicmake\fP \fI\-\-source\fP compiles the \fBicmake\fP script to a temporary \fI\&.bim\fP
file;
.IP o 
the support program \fIicmun\fP was renamed to \fBicm\-un\fP (called by
\fBicmake\fP when specifying the \fI\-\-unassemble\fP (\fI\-u\fP) option, so in
practice the name change is automatically handled)\&.
.IP o 
The \fBicmconf\fP(7) script can use \fI\-o\fP in its \fI#define SPCH\fP
specification\&. All (space character delimited) words following \fI\-o\fP
are passed to \fBicm\-spch\fP as separate `\fI\-o word\fP\(cq\& options;
.IP o 
When developing \fBC++\fP programs the environment variable
\fIICMAKE_CXXFLAGS\fP is no longer used\&. Instead the environment
variable \fIICMAKE_CPPSTD\fP is used\&. Use this latter environment
variable to define one point of maintenance specifying the version of
the \fBC++\fP standard used when compiling sources;
.IP o 
When using Debian (or a comparable distributions) the script
\(cq\&rebuild\(cq\& can be used to rebuild icmake\(cq\&s binaries after installing
\(cq\&libbobcat\-dev\(cq\&, resulting in a reduction of their sizes of about 40%\&.
.IP o 
The \fBicmake\fP program itself was redesigned\&.

.PP 
.SH "OPTIONS"

.PP 
Where available, single letter options are listed between parentheses
beyond their associated long\-option variants\&. \fBIcmake\fP defines \fIaction\fP options
and \fInon\-action\fP options\&. The first action option that is encountered is
used\&.
.PP 
When using \fBicmbuild\fP(1) for program maintenance \fBicmake\fP is called
automatically, and the user doesn\(cq\&t have to specify any \fBicmake\fP options\&.
.PP 
.IP o 
\fB\-\-about\fP (\fB\-a\fP)
.br 
Ends \fBicmake\fP after showing some information about \fBicmake\fP;
.IP 
.IP o 
\fB\-\-compile\fP (\fB\-c\fP) \fI[options] source [bim\-file]\fP 
.br 
The \fBicmake\fP script is first pre\-processed (see option \fI\-\-preprocess\fP
below) whereafter the pre\-processed file is compiled by \fIicm\-comp\fP
producing a \fIbim\-file\fP\&. If the \fIbim\-file\fP name is not specified
then \fIsource\(cq\&s\fP base\-name, receiving extension \fI\&.bim\fP, is
used\&.
.br 
If \fIsource\fP is a previously pre\-processed file then option \fI\-P\fP
can be specified to suppress its pre\-processing\&. E\&.g\&.,
.nf 

    icmake \-c \-P source dest\&.bim
       
.fi 
If the bim\-file exists and is younger than \fIsource\fP then \fIsource\fP
is not compiled;
.IP 
.IP o 
\fB\-\-dependencies\fP (\fB\-d\fP) \fI[options] action\fP
.br 
The \fBicm\-dep\fP program is called determining the dependencies among
classes\&. All options and arguments following this option are forwarded
to \fBicm\-dep\fP\&. Refer to the \fBICM\-DEP\fP section of this man\-page for
information about \fIicm\-dep\fP;
.IP 
.IP o 
\fB\-\-execute\fP (\fB\-e\fP) \fI[option] bim\-file [arguments]\fP
.br 
Executes the bim\-file, Before specifying \fIbim\-file\fP option
\fI\-\-no\-version\-check\fP (see below, or the equivalent short option
\fI\-n\fP) can be specified to allow mismatches between \fBicmake\fP\(cq\&s main
version and the \fBicmake\fP version that was used to compile the
bim\-file\&.
.br 
Command\-line arguments specified beyond \fIbim\-file\fP are forwarded as
arguments to the \fIbim\-file\(cq\&s main\fP function (cf\&. the
\fBicmscript\fP(7) man\-page for details about writing \fBicmake\fP\-scripts);
.IP 
.IP o 
\fB\-\-force\fP (\fB\-f\fP) \fI[options] source [bim\-file]\fP 
.br 
Acts like option \fI\-\-compile\fP, but compilation is always performed,
even if the bim\-file is up\-to\-date;
.IP 
.IP o 
\fB\-\-help\fP (\fB\-h\fP)
.br 
Ends \fBicmake\fP after providing usage info\&. Usage info is also provided when
\fBicmake\fP is started without arguments;
.IP 
.IP o 
\fB\-\-multicomp\fP (\fB\-m\fP) \fI[options] jobs \(cq\&compiler\-spec\(cq\&\fP 
.br 
The optional \fIoptions\fP are the options passed to the
\fIicm\-multicomp\fP program (cf\&. section \fBICM\-MULTICOMP\fP below)\&.
.br 
See also the \fBicomonf\fP(7) man\-page\(cq\&s \fI#define MULTICOMP\fP directive:
when specified threaded compilation is automatically used;
.IP 
.IP o 
\fB\-\-no\-process\fP (\fB\-N\fP)
.br 
Implies option \fI\-\-verbose\fP (see below)\&. This option is recognized by
options \fI\-\-dependencies, \-\-execute, \-\-source\fP and \fI\-t\fP (either as
two separate options or by `gluing\(cq\& both options together, like
\fI\-Ne\fP)\&. When specified, the support program is not run, but the
command(s) that would have been used are shown to the standard output;
.IP 
.IP o 
\fB\-\-no\-version\-check\fP (\fB\-n\fP)
.br 
This option is available with the action options \fI\-\-execute,
\-\-source, \-\-unassemble\fP, and \fI\-t\fP\&. When specified the main versions
stored in the specified icm\-bim file and \fBicmake\fP itself may differ\&. This
option should normally not be required, but was primarily added for
development purposes;
.IP 
.IP o 
\fB\-\-preprocess\fP (\fB\-p\fP)  \fI[options] source [pim\-file]\fP 
.br 
The file specified as first argument is pre\-processed, producing a
`\fI\&.pim\fP\(cq\& file\&. If a second filename argument is provided then that
file becomes the \fI\&.pim\fP file\&. If not specified, then
the first filename, using the extension \fI\&.pim\fP, is used\&. 
.br 
With this option pre\-processor symbol\-defining options can be used:
symbols whose values can be used in \fIsource\fP\&. E\&.g\&., when
issuing the command
.nf 

    icmake \-p \-d one \-\-define two source dest\&.pim
       
.fi 
\fBicmake\fP pre\-processes \fIsource\fP, defines the pre\-processor symbols
\fIone\fP and \fItwo\fP (each having value 1), and produces the pim\-file
\fIdest\&.pim\fP\&. Note that instead of using long options \fI\-\-define\fP
short options \fI\-d\fP can also be used;
.IP 
.IP o 
\fB\-\-spch\fP (\fB\-S\fP) \fI\&.\&.\&.\fP 
.br 
A SPCH is built\&. All options and arguments following \fI\-\-spch\fP are
forwarded to the \fIicm\-spch\fP support program\&.  (cf\&. section
\fBICM\-SPCH\fP below)\&.
.br 
See also the \fBicomonf\fP(7) man\-page\(cq\&s \fI#define SPCH\fP directive: when
specified a SPCH is automatically constructed;
.IP 
.IP o 
\fB\-\-source\fP (\fB\-s\fP)  \fI[options] source [arguments]\fP 
.br 
\fBIcmake\fP uses \fI\-\-compile\fP to compile the \fBicmake\fP source file specified as
first argument (constructing a temporary bim\-file) and
then uses \fI\-\-execute\fP to execute the bim\-file, forwarding any
subsequent \fIarguments\fP as arguments to the \fIbim\-file\(cq\&s main\fP
function\&.
.br 
Following the \fI\-\-source\fP option options available for \fBicmake\fP\(cq\&s
\fI\-\-compile\fP command can be specified\&. Following those options 
options of \fBicmake\fP\(cq\&s \fI\-\-execute\fP options can be specified\&. Those
latter options must be preceded by \fI\-\-execute\fP or \fI\-e\fP\&. E\&.g\&., when
issuing the command
.nf 

    icmake \-s \-d one \-en source 
       
.fi 
then \fBicmake\fP first compiles \fIsource\fP after defining the pre\-processor
symbol \fIone\fP, and then executes the bim\-file, passing
\fI\-\-no\-version\-check\fP to \fIicm\-exec\fP;
.IP 
.IP o 
\fB\-t\fP \fItmpspec\fP \fI[options] source [arguments]\fP 
.br 
This option is intended for \fBicmake\fP\-scripts although it can also be used
in a command\-line \fBicmake\fP call\&. The \fI\-t\fP option can be used in
quite a few different ways which is covered in the next section
(\fBEXECUTING ICMAKE SCRIPTS\fP)\&.
.IP 
.IP o 
\fB\-\-tmpdir\fP=\fIdirectory\fP (\fB\-T\fP)
.br 
By default temporary files are created in the \fI/tmp\fP sub\-directory,
or in the user\(cq\&s home (\fI${HOME}\fP) directory if \fI/tmp/\fP cannot be
used\&. To specify another directory to write temporary files in \fBicmake\fP\(cq\&s
option \fI\-\-tmpdir\fP (or \fI\-T\fP) can be specified, followed by the name
of an (existing) directory to use for temporary files\&.
.br 
E\&.g\&., when compiling an \fBicmake\fP file \fImain\&.im\fP, the output of \fBicmake\fP\(cq\&s
pre\-processor is written to a temporary file which is removed after
compilation\&. To write the temporary file to \fI~/\&.icmake\fP \fBicmake\fP can be
called as
.nf 

    icmake \-T ~/\&.icmake \-c main\&.im
        
.fi 

.IP 
.IP o 
\fB\-\-unassemble\fP (\fB\-u\fP)
.br 
The file specified as first argument is an \fBicmake\fP bim\-file, which is
unassembled (cf\&. section \fBICM\-UN\fP below for more information about
unassembling \fBicmake\fP bim\-files);
.IP 
.IP o 
\fB\-\-verbose\fP (\fB\-V\fP)
.br 
\fBIcmake\fP child processes and their arguments are written to the standard
output stream before they are called\&. This option may precede `action\(cq\&
options (\fI\-c, \-d, \-e, \-s\fP and \fI\-u\fP), either as two separate
options or by `gluing\(cq\& both options together, like \fI\-Ve\fP\&.
.IP 
.IP o 
\fB\-\-version\fP (\fB\-v\fP)
.br 
Ends \fBicmake\fP after displaying its version\&.

.PP 
.SH "EXECUTING ICMAKE SCRIPTS"

.PP 
The \fI\-t\fP option is available primarily for being used in executable \fBicmake\fP
scripts\&. The first line of executable \fBicmake\fP scripts consists of the following
components:
.IP o 
a \fIshebang\fP (\fI#!\fP) specification followed by \fBicmake\fP\(cq\&s location;
.IP o 
the \fI\-t\fP option followed by its argument (see below);

.PP 
and optionally:
.IP o 
compilation and execution options (specified as described at the
\fI\-\-source\fP option);
.IP o 
a non\-option component (e\&.g\&., \fI:\fP) which will be replaced by the
location of the executable \fBicmake\fP script;
.IP o 
remaining components on the first line are forwarded as\-is to the
\fBicmake\fP script\(cq\&s \fImain\fP function;

.PP 
E\&.g\&., the following simple \fIicm\fP executable script `\fIargs\fP\(cq\& could be
defined in \fI~/bin\fP:
.nf 
    #!/usr/bin/icmake \-t\&. \-d one : two \-\-dir three
    void main(int argc, list argv)
    {
        #ifdef one
            printf << \(dq\&one is defined\en\(dq\&;
        #endif
        printf << argc << \(dq\& arguments: \(dq\& << argv << \(cq\&\en\(cq\&;
    }
        
.fi 
When it\(cq\&s called as \fI~/bin/args four five\fP then the arguments \fIfour\fP
and \fIfive\fP are added to the \fItwo \-\-dir three\fP arguments already specified
by the script itself\&. Called this way the script outputs\&. Its 1st argument is
the name of a temporary \fI\&.bim\fP file which is different at each new call:
.nf 
    one is defined
    /tmp/F04dgh two \-\-dir three four five
        
.fi 

.PP 
The argument of the \fI\-t\fP option is used to specify the location of the files
used by \fBicmake\fP\(cq\&s \fI\-t\fP option (\fI/dir\fP can also be specified as multiple
directories, like \fI/dir/sub1/sub2\fP):
.IP o 
if it\(cq\&s a single dot (\fI\-t\&.\fP) the compiled (temporary) bim\-file is
written in \fBicmake\fP\(cq\&s directory used for temporary files (e\&.g\&., \fI/tmp\fP
or \fI$HOME\fP);
.IP o 
if it\(cq\&s \fI~/dir\fP or \fI/dir\fP then \fBicmake\fP writes temporary files in
the specified directory, replacing \fI~\fP by the user\(cq\&s \fI$HOME\fP
directory;
.IP o 
if it\(cq\&s \fI~/dir/name\fP or \fI/dir/name\fP then \fI[~]/dir/name\fP
specifies the path of the compiled \fI\&.bim\fP file, writing all other
temporary files in the same directory as the \fI\&.bim\fP file\&. The
\fI\&.bim\fP file is kept after execution (allowing it to be executed
repeatedly using \(cq\&\fBicmake\fP \fI\-e\fP\(cq\&)\&. The \fBicmake\fP\-script is not compiled if
the \fI\&.bim\fP file is younger than the script\&.

.PP 
.SH "ICM\-DEP"

.PP 
\fBIcm\-dep\fP is a support program called by \fBicmake\fP \fI\-\-dependencies\fP (or its
short alternative \fI\-d\fP) to determine source\- and precompiled\-header file
dependencies\&. 
.PP 
When using \fBicmbuild\fP(1) and an \fIicmconf\fP file contains the \fI#define
SPCH\fP directive \fBicmake\fP \fI\-d\fP is not used; instead, directories inspected when
using a SPCH are automatically inspected (and updated when necessary) by
\fBicmake\fP \fI\-S\fP\&.
.PP 
\fBIcmake\fP \fI\-d\fP\(cq\&s synopsis is
.nf 
    icmake \-d [options] argument
.fi 
Specifications following \fI\-d\fP are forwared to \fBicm\-dep\fP\&. The following
options can be specified after \fI\-d\fP:
.IP o 
\fB\-\-classes\fP=\fIfilename\fP (\fB\-c\fP)
.br 
By default, \fBicm\-dep\fP inspects dependencies of the directories
mentioned in the file \fICLASSES\fP\&. If the \fBicmconf\fP(7)
file specifies \fIPARSER_DIR\fP and/or \fISCANNER_DIR\fP then those
directories are also considered\&.  Use this option to specify the file
containing the names of directories to be inspected by \fBicm\-dep\fP\&.
.IP 
.IP o 
\fB\-\-gch\fP
.br 
If an \fIicmconf\fP file specifies the (deprecated) \fI#define PRECOMP\fP
directive then \fBicmake\fP \fI\-d\fP checks whether precompiled headers must be
refreshed\&.  If an \fIicmconf\fP file does not contain a \fI#define
PRECOMP\fP diretive, but precompiled headers should nonetheless be
inspected, then option \fI\-\-gch\fP can be specified;
.IP 
.IP o 
\fB\-\-help\fP (\fB\-h\fP)
.br 
A summary of \fBicm\-dep\fP\(cq\&s usage is written to the standard output and
\fBicmake\fP terminates, returning 0 to the operating system;
.IP 
.IP o 
\fB\-\-icmconf\fP=\fIfilename\fP (\fB\-i\fP)
.br 
By default \fBicmake\fP \fI\-d\fP inspects the content of \fIicmconf\fP files, This
option is used if instead of \fIicmconf\fP another file should be
inspected;
.IP 
.IP o 
\fB\-\-mainih\fP=\fImainheader\fP (\fB\-m\fP)
.br 
In the \fIicmconf\fP file the \fI#define IH\fP directive is used to specify
the suffix of class header files that should be precompiled, assuming
that their filenames are equal to the names of the directories which
are listed in the \fICLASSES\fP file\&. But \fICLASSES\fP does not specify
the name of the program\(cq\&s top\-level directory\&. This option is used to
specify the name of the top\-level header file to precompile\&. By
default \fImain\&.ih\fP is used;
.IP 
.IP o 
\fB\-\-no\-gch\fP
.br 
If an \fIicmconf\fP file contains a (deprecated) \fI#define PRECOMP\fP
directive but \fBicmake\fP \fI\-d\fP should not check whether precompiled
headers must be refreshed then specify option \fI\-\-no\-gch\fP;
.IP 
.IP o 
\fB\-\-no\-use\-all\fP
.br 
If this option is specified then the \fI#define USE_ALL \(dq\&filename\(dq\&\fP
directive in the \fIicmconf\fP file (cf\&. \fBicmconf\fP(7)) is ignored;
.IP 
.IP o 
\fB\-\-use\-all\fP=\fIfilename\fP
.br 
If this option is specified then all files in directories containing
the file \fIfilename\fP and source files in directories (recursively)
depending on the files in those directories are recompiled;
.IP 
.IP o 
\fB\-\-verbose\fP (\fB\-V\fP)
.br 
This option can be specified multiple times\&. The number of times it is
specified determines the verbosity of \fBicmake\fP \fI\-d\fP\(cq\&s output\&. If not
specified then \fBicmake\fP \fI\-d\fP silently performs its duties\&. If specified
once (which is the default specification), then \fBicmake\fP \fI\-d\fP writes to
the standard output the actions it performs; if specified twice it
also reports non\-default options and automatically included
directories; if specified three times it also reports class
dependencies; if specified more often it reports what files it
encountered and what decision it would make when \fIgo\fP would be
specified;
.IP 
.IP o 
\fB\-\-version\fP (\fB\-v\fP)
.br 
\fBIcmake\fP \fI\-d\fP writes \fBicm\-dep\fP\(cq\&s version number to the standard
output and terminates, returning 0 to the operating system\&.

.PP 
Following the non\-terminating options argument \fIgo\fP must be specified to
allow \fBicmake\fP \fI\-d\fP to perform its actions\&. Specifying another argument results
in a `dry run\(cq\&: it analyzes dependencies, but won\(cq\&t remove or touch files\&.
.PP 
\fBIcmake\fP \fI\-d\fP can be used for software projects which are developed as
described in the
C++ Annotations, section
\fIHeader file organization\fP in chapter \fIClasses\fP\&. For those projects
classes are developed in their own directories, which are direct
sub\-directories of the project\(cq\&s main program directory\&. Their class
interfaces are provided in class\-header files bearing the names of the
class\-directories, and all headers that are required by the class\(cq\&s sources
are declared in a separate \fIinternal header\fP files, commonly having
extensions \fI\&.ih\fP\&.
.PP 
.SH "ICM\-MULTICOMP"

.PP 
\fBIcmake\fP supports multi\-threaded source\-file compilation, often significantly
reducing the compilation time of the source files of projects\&. When using
\fBicmbuild\fP(1) multi\-threaded compilation is automatically used when
\fIicmconf\fP files contain the \fI#define MULTICOMP\fP directive
(cf\&. \fBicmconf\fP(7))\&. It can also be called independently from \fIicmconf\fP
using \fBicmake\fP\(cq\&s \fI\-\-multicomp\fP (or \fI\-m\fP) option\&.
.PP 
\fBIcmake\fP \fI\-m\fP\(cq\&s synopsis is
.nf 
    icmake \-m [options] arguments
.fi 

.PP 
\fBIcmake\fP \fI\-m\fP accepts the following options:
.IP o 
\fB\-\-help\fP (\fB\-h\fP)
.br 
\fBIcmake\fP \fI\-m\fP writes a summary of its usage to the standard output
and terminates, returning 0 to the operating system;
.IP 
.IP o 
\fB\-\-nr\fP (\fB\-n\fP)
.br 
When compiling source files and option \fI\-\-nr\fP is specified then the
thread number compiling a source file is written to the standard
output stream\&.
.IP 
.IP o 
\fB\-\-quiet\fP (\fB\-q\fP)
.br 
When this options is not specified then the path names of the
compiled object and source files are written to the standard output
stream\&. When it is specified once only the source files\(cq\& directories
and filenames are written to the standard output stream, and when it
is specified more than once no information about the compiled files is
written to the standard output stream\&.
.IP 
.IP o 
\fB\-\-threads\fP=\fInThreads\fP (\fB\-t\fP)
.br 
By default the computer\(cq\&s number of cores determines the number of
threads being used when compiling the source files\&. A different number
of threads can be requested using this option, e\&.g\&., \fI\-\-threads 5\fP\&.
.IP 
.IP o 
\fB\-\-version\fP (\fB\-v\fP)
.br 
\fBIcmake\fP \fI\-m\fP reports its version number to the standard output and
terminates, returning 0 to the operating system\&.

.PP 
\fBIcmake\fP \fI\-m\fP needs one command\-line argument and an optional second
argument: 
.IP o 
the first argument is the name of the file specifying which files
must be compiled\&. Use \fBicmbuild\fP(1) to write this file\&. It can also
be constructed otherwise:
.br 
The specified file must contain groups of file specifications where
each group starts with a line like \fI: support tmp/o 5\fP where the 2nd
element (here: \fIsupport\fP) specifies the (sub\-)directory of the
source files (use \fI\&.\fP to refer to the project\(cq\&s top\-level
directory); the 3rd element (here: \fItmp/o\fP) specifies the
destination directory of the compiled files (which is created if not
existing); and the 4th element (here: \fIt\fP) specifies the prefix to
add in front of the compiled object files\&.
.br 
Following this line the remaining lines of a group specify the names of
the source files (located in specified (sub\-)directory) to compile\&.
.br 
Once the compilation ends (either because all files were successfully
ccompiled, or because a compilation failed) the specification file is
removed;
.IP o 
the second argument is optional\&. By default the following
specification is used  (all on one line)
.nf 

    g++ \-c \-o $2 ${ICMAKE_CPPSTD} \-\-Wall \-Werror $1
       
.fi 
Here \fI$1\fP is replaced by the location of the source file to compile
and \fI$2\fP is replaced by the location of the compiled object file\&. If
the environment variable \fIICMAKE_CPPSTD\fP is defined (specifying the
\fBC++\fP standard to use, e\&.g\&., \fIICMAKE_CPPSTD=\-\-std=c++26\fP) then its
value replaces \fI${ICMAKE_CPPSTD}\fP in the specification\&.
.br 
If the default compiler specification cannot be used the command to
compile source files can be provided as \fBicmake\fP \fI\-m\fP\(cq\&s second
command\-line argument, which should be quoted, like
.nf 

    \(cq\&g++ \-c \-o $2 \(cq\&${ICMAKE_CPPSTD}\(cq\& \-\-Wall \-Werror $1\(cq\&
       
.fi 
or the second command\-line argument can be \fIf:file\fP, where \fIfile\fP
is the name of a file whose first line contains the specification of
the command compiling source files (which must specify \fI$1\fP and
\fI$2\fP and optionally \fI$ICMAKE_CPPSTD\fP)\&.
.br 
The \fIPATH\fP environment variable is used to locate
the compiler; the compiler\(cq\&s absolute path can also be specified\&.

.PP 
.SH "ICM\-SPCH"

.PP 
Single Pre\-Compiled Headers (SPCH) are available using \fBicmake\fP \fI\-\-spch\fP
(or \fI\-S\fP)\&. 
.PP 
\fBIcmake\fP \fI\-S\fP\(cq\&s synopsis is
.nf 
    icmake \-S [options] arguments
.fi 

.PP 
\fBIcmake\fP \fI\-S\fP accepts the following options:
.IP o 
\fB\-\-all\fP=\fIfile\fP (\fB\-a\fP)
.br 
Call in sequence \fBicm\fP \fI\-S\fP\(cq\&s options \fI\-\-list, \-\-precompile\fP, and
\fI\-\-soft\-links\fP (see below), where \fIfile\fP is written by option
\fI\-\-list\fP and used as option argument for \fI\-\-precompile\fP and
\fI\-\-soft\-links\fP;
.IP 
.IP o 
\fB\-\-classes\fP=\fIfilename\fP (\fB\-c\fP)
.br 
The file \fIfilename\fP is the name of a file containing a list of
directories inspected by the \fI\-\-list\fP option (by default
CLASSES)\&. The project\(cq\&s top directory is automatically inspected
unless the option \fI\-\-no\-topdir\fP is specified;
.IP 
.IP o 
\fB\-\-help\fP (\fB\-h\fP)
.br 
\fBIcmake\fP \fI\-S\fP writes a summary of its usage to the standard output
and terminates, returning 0 to the operating system;
.IP 
.IP o 
\fB\-\-internal\fP=\fI\&.ext\fP (\fB\-i\fP)
.br 
\fI\&.ext\fP is the extension used for the internal headers (including the
dot)\&. By default \fI\&.ih\fP is used;
.IP 
.IP o 
\fB\-\-list\fP (\fB\-l\fP)
.br 
Ignored when option \fI\-\-all\fP is specified\&.
.br 
Write the names of the files to process when constructing an SPCH
to the file specified as \fBicmake\fP \fI\-S\fP\(cq\&s first command line
argument\&. The specified filename may not have an extension or
directory specifications (e\&.g\&., \fIspch\fP);
.IP 
.IP o 
\fB\-\-no\-topdir\fP (\fB\-n\fP)
.br 
Ignore the internal header found in the project\(cq\&s top directory\&. This
option is used when, e\&.g\&., merely constructing a library instead of a
program;
.IP 
.IP o 
\fB\-\-precompile\fP=\fIfile\fP (\fB\-p\fP)
.br 
Precompile \fIfile\fP (the name of the file specified at the option
\fI\-\-list\fP) to the SPCH file specified as \fBicmake\fP \fI\-S\fP\(cq\&s first
command\-line argument\&. If that argument ends in \fI/\fP then the SPCH
\fI\(cq\&argument\(cq\&file\&.gch\fP is written\&.
.br 
By default the SPCH is constructed by the following command (all on
one line):
.nf 

    g++ \-c \-o $2 ${ICMAKE_CPPSTD} \-Wall \-Werror \-O2 \-x c++\-header $1
       
.fi 
Here, $1 is replaced by \(cq\&file\(cq\&, and $2 is replaced by the name of the
SPCH, while \fI$ICMAKE_CPPSTD\fP refers to the value of the
\fIICMAKE_CPPSTD\fP environment variable (specifying the \fBC++\fP
standard to use, e\&.g\&., ICMAKE_CPPSTD=\-\-std=c++26)\&.
.br 
Alternatively, the command constructing the SPCH can be provided as
second command\-line argument, which should be quoted like
.nf 

    \(cq\&g++ \-c \-o $2 \(cq\&${ICMAKE_CPPSTD}\(cq\& \-Wall \-Werror \-O2 \-x c++\-header $1\(cq\&
       
.fi 
or the second command\-line argument can be \fIf:file\fP, where \fIfile\fP
is the name of a file whose first line specifies the command
constructing the SPCH (which must specify \fI$1\fP and \fI$2\fP and
optionally \fI$ICMAKE_CPPSTD\fP)\&.
.br 
The \fIPATH\fP environment variable is used to locate
the compiler; the compiler\(cq\&s absolute path can also be used\&.
.IP 
.IP o 
\fB\-\-quiet\fP (\fB\-q\fP)
.br 
By default the (\fIg++\fP) command constructing the single precompiled
header file is echoed to the standard output stream\&. Specify this
option to suppress writing the command to the standard output stream\&.
.IP 
.IP o 
\fB\-\-soft\-links\fP=\fIfile\fP (\fB\-s\fP)
.br 
Ignored when option \fI\-\-all\fP is specified\&.
.br 
This option uses the same arguments as used with the \fI\-\-precompile\fP
option\&. This option creates \fI\&.gch\fP soft\-links from the header files
listed in \fIfile\fP to the generated SPCH\-file;
.IP 
.IP o 
\fB\-\-unused\fP=\fIregex\fP (\fB\-u\fP)
.br 
In practice this option is probably not required, but files matching
(POSIX extended) \fIregex\fP are not inspected by \fBicmake\fP \fI\-S\fP\&. When
used specifications like \fI(\&.\&.\&.)|(\&.\&.\&.)\fP can be used to specify
multiple regexes\&. Alternatively \fIf:file\fP can be used as option
argument to specify a file whose non\-empty lines contain regexes;
.IP 
.IP o 
\fB\-\-version\fP (\fB\-v\fP)
.br 
\fBIcmake\fP \fI\-S\fP writes its version number to the standard output and
terminates, returning 0 to the operating system;
.IP 
.IP o 
\fB\-\-warn\fP (\fB\-w\fP)
.br 
interactively warn when existing header files are about to be modified,
accepting or refusing the modifications\&. Once refused \fBicm\-spch\fP
ends\&. 

.PP 
\fBIcmake\fP \fI\-S\fP needs one, and optionally two arguments, which were described at
the \fI\-\-list, \-\-precompile\fP, and \fIsoft\-links\fP option descriptions\&.
.PP 
Pre\-compiled headers have been available for quite some time, and usually
result in a significant reduction of the compilation time\&. Using single
precompiled headers results in a large reduction of required disk\-space
compared to using precompiled headers for separate directories\&.
.PP 
When using SPCHs almost identical precompiled headers for separate directories
are avoided: only one precompiled header is constructed which is then used by
all components of a project\&. As identical sections are avoided the sizes (and
construction times) of SPCHs are much smaller, usually requiring only 5 to 10 %
of the space (and construction time) ccompared to using separately constructed
pre\-compiled headers\&.
.PP 
SPCHs can easily be used in combination with \fBicmbuild\fP(1)\&. Often a
specification in a project\(cq\&s \fIicmconf\fP file like \fI#define SPCH \(dq\&\(dq\&\fP is all
it takes (cf\&. \fBicmconf\fP(7))\&.
.PP 
.SH "ICM\-UN"

.PP 
The support program \fBicm\-un\fP is called by \fBicmake\fP \fI\-u\fP, expecting one
argument, a bim\-file\&. It disassembles the binary file an shows the assembler
instructions and the structure of the bim\-file\&.
.PP 
As an illustration, assume the following script is compiled 
by \fIicmake \-c demo\&.im\fP):
.nf 

    void main()
    {
        printf << \(dq\&hello world\en\(dq\&;
    }
        
.fi 
the resulting \fIdemo\&.bim\fP file is disasembled by \fBicmake\fP \fI\-u demo\&.bim\fP
writing the following to the standard output fle:
.nf 

    icm\-un by Frank B\&. Brokken (f\&.b\&.brokken@rug\&.nl)
    icm\-un V12\&.04\&.00
    Copyright (c) GPL 1992\-2024\&. NO WARRANTY\&.
    
    Binary file statistics:
        strings      at offset  0x0025
        variables    at offset  0x0033
        filename     at offset  0x0033
        code         at offset  0x0014
        first opcode at offset  0x0021
    
    String constants dump:
        [0025 (0000)] \(dq\&\(dq\&
        [0026 (0001)] \(dq\&hello world\&.\(dq\&
    
    Disassembled code:
        [0014] 06 01 00   push string \(dq\&hello world\&.\(dq\&
        [0017] 05 01 00   push int 0001
        [001a] 1b 1d      callrss 1d (printf)
        [001c] 1c 02      add sp, 02
        [001e] 04         push int 0
        [001f] 24         pop reg
        [0020] 23         ret
        [0021] 21 14 00   call [0014]
        [0024] 1d         exit

.fi 

.PP 
Offsets are shown using the hexadecimal number system and are absolute byte
offsets in the bim\-file\&. The string constants dump also shows, between
parentheses, the offsets of the individual strings relative to the beginning
of the strings section\&. The output also shows the opcodes of the instructions
of the compiled \fI\&.im\fP source files\&. If opcodes use arguments then these
argument values are shown following their opcodes\&. Each opcode line ends by
showing the opcode\(cq\&s mnemonic plus (if applicable) the nature of its argument\&.
.PP 
.SH "FILES"

.PP 
The mentioned paths are the ones that are used in the source distribution
and are used by the Debian Linux distribution\&. However, they are sugestive
only and may have been configured differently:
.PP 
Binary programs:
.IP o 
\fB/usr/bin/icmake\fP: the main \fBicmake\fP program;
.IP o 
\fB/usr/bin/icmbuild\fP: the wrapper program around the \fIicmbuild\fP
script handling standard program maintenance;
.IP o 
\fB/usr/bin/icmodmap\fP: the module mapper which initializes the
maintenance of \fBC++\fP programs using modules;
.IP o 
\fB/usr/bin/icmstart\fP: an \fBicmake\fP\-script that is can be used to create
the startup\-files of new projects;

.PP 
Support programs:
.PP 
.IP o 
\fB/usr/libexec/icmake/icm\-comp\fP: the compiler called by \fBicmake\fP;
.IP o 
\fB/usr/libexec/icmake/icm\-dep\fP: the support program handling
class\-dependencies;
.IP o 
\fB/usr/libexec/icmake/icm\-exec\fP: the byte\-code interpreter;
.IP o 
\fB/usr/libexec/icmake/icm\-multicomp\fP: the multi\-thread source file
compiler; 
.IP o 
\fB/usr/libexec/icmake/icm\-pp\fP: the preprocessor called by \fBicmake\fP;
.IP o 
\fB/usr/libexec/icmake/icm\-spch\fP: the program preparing SPCHs;
.IP o 
\fB/usr/libexec/icmake/icm\-un\fP: the \fBicmake\fP unassembler\&.

.PP 
.SH "EXAMPLES"

.PP 
The distribution (usually in \fI/usr/share/doc/icmake\fP) contains a
directory \fIexamples\fP containing additional examples of \fBicmake\fP script\&. The
\fIicmstart\fP script is an \fBicmake\fP script as is \fI/usr/libexec/icmake/icmbuild\fP,
which is called by the \fI/usr/bin/icmbuild\fP program\&. See also the \fBEXAMPLE\fP
section in the \fBicmscript\fP(7) man\-page\&.
.PP 
.SH "SEE ALSO"
\fBchmod\fP(1),
\fBicmbuild\fP(1), \fBicmconf\fP(7), \fBicmodmap\fP(1), \fBicmscript\fP(7),
\fBicmstart\fP(1), \fBicmstart\&.rc\fP(7), \fBmake\fP(1)
.PP 
.SH "BUGS"

.PP 
None reported\&.
.PP 
.SH "COPYRIGHT"
This is free software, distributed under the terms of the 
GNU General Public License (GPL)\&.
.PP 
.SH "AUTHOR"
Frank B\&. Brokken (\fBf\&.b\&.brokken@rug\&.nl\fP)\&.
.PP 
