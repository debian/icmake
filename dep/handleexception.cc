#include "main.ih"

int handleException()
try
{
    rethrow_exception(current_exception());
} 
catch(int x)
{
    return Arg::instance().option("hv") ? 0 : x;
}
catch (exception const &exc)
{
    cerr << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cerr << "unexpected exception\n";
    return 1;
}
