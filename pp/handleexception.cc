#include "main.ih"

int handleException()
try
{
    rethrow_exception(current_exception());
} 
catch (int retValue)
{
    return retValue;
}
catch (exception const &exc)
{
    cerr << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cerr << "fatal: unexpected exception\n";
    return 1;
}

