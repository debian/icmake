#include "main.ih"

int handleException()
try
{
    rethrow_exception(current_exception());
} 
catch (exception const &exc)
{
    cout << '\n' <<
            exc.what() << '\n';
    return 1;
}
catch (int exitValue)
{
    return Arg::instance().option("hv") ? 0 : exitValue;
}
catch (...)
{
    return 1;
}
