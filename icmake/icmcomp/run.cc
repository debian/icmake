#define XERR
#include "icmcomp.ih"

    // when --source calls icm-comp then --source prepares the
    // correct arguments for icm-comp: a 1st and 2nd filename after the
    // compilation options.
    // icm-comp therefore inspects argv()[1] and argv()[2]

int IcmComp::run()
{
    d_execute.init("icm-comp", 
                   d_preCompOpts.usage(), d_preCompOpts.version());

                                                        // get the plain args
    OptBase::StrVect const &args = d_preCompOpts.plainArgs();

    string bimFile = 
              args.size() > 1 ? args[1] : Base::useExt(args.front(), ".bim") ;
    return
        not doCompile(args.front(), bimFile) ?  // maybe no need to compile
            0 
        :
            d_preCompOpts.preProcessed() ?      // alreadt preprocessed ?
                compile(args.front(), bimFile)  // then only compile
            :
                ppAndComp(args.front());        // else both prep. and comp.
}




