#ifndef INCLUDED_ICMCOMP_
#define INCLUDED_ICMCOMP_

#include <iosfwd>

#include "../base/base.h"
#include "../execute/execute.h"

class PreCompOpts;
class OptBase;

class IcmComp
{
    PreCompOpts &d_preCompOpts;
    Execute d_execute;
    Base::ExecType d_type;           // Execute as CHILD or DIRECT
    bool d_skipCompile;
        // ScriptOpts sets skipCompile at -tdirname if the .bim file is 
        // younger than the script, by default: false -> maybe compile
    
    public:
                                // ScriptOpts sets dontCompile at -tdirname
                                // if the .bim file is younger than the script
        IcmComp(OptBase &optBase, Base::ExecType type, 
                bool skipCompile = false);
        int run();

    private:
        bool doCompile(std::string const &src, 
                       std::string const &bimFile) const;

        int ppAndComp(std::string imFile);
        int compile(std::string const &src, std::string const &dest);

};
        
#endif
