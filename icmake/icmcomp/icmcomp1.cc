#define XERR
#include "icmcomp.ih"

IcmComp::IcmComp(OptBase &optBase, Base::ExecType type, bool skipCompile)
:
    d_preCompOpts(dynamic_cast<PreCompOpts &>(optBase)),
    d_type(type),
    d_skipCompile(skipCompile)
{}
