#define XERR
#include "icmcomp.ih"

    // ScriptOpts sets dontCompile at -tdirname if the .bim file is 
    // younger than the script

bool IcmComp::doCompile(string const &src, string const &bimFile) const
{
    Stat statBim{bimFile};

    return
        d_skipCompile ?
            false
        :
            d_preCompOpts.forced()  or 
            not statBim             or
            statBim.lastModification() <= Stat{src}.lastModification();
}
