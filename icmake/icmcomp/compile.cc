#define XERR
#include "icmcomp.ih"

int IcmComp::compile(string const &src, string const &dest)
{
    return d_execute.run("icm-comp " + src + ' ' + dest, d_type);
}
