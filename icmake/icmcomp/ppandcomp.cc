#define XERR
#include "icmcomp.ih"

int IcmComp::ppAndComp(string imFile)
{
    TempStream pim{ Base::tmpDir() };   // setPlain may redefine 
                                        // OptBase's d_plainArgs
    string plain1 = d_preCompOpts.setPlain(1, pim.fileName());

    IcmPp icmPp{ d_preCompOpts, Base::CHILD };
    
    int ret = icmPp.run();

    return 
        ret != 0 ? 
            ret 
        : 
            compile(
                pim.fileName(), 
                plain1.empty() ? Base::useExt(imFile, ".bim") : plain1 
            );
}
