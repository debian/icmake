#define XERR
#include "args.ih"

char *Args::add(string const &str)
{
    d_strPtrVect.push_back(new string{ str });
    return &d_strPtrVect.back()->front();
}
