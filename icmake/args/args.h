#ifndef INCLUDED_ARGS_
#define INCLUDED_ARGS_

#include <vector>
#include <string>
#include <memory>

    // strings are added to a vector, returning their 1st char. as
    // a (modifiable) NTBS which can then be used like main's argv

class Args
{
    using StrPtrVect = std::vector<std::string *>;

    StrPtrVect d_strPtrVect;

    static std::unique_ptr<Args> s_args;
    
    public:
        Args(Args const &other) = delete;
        ~Args();

        static Args &instance();                                    // inline
        char *add(std::string const &str);          // 1.cc
        char *add(std::string &&tmp);               // 2.cc

        std::string &last();                        // inline

    private:
        Args() = default;
};
        
inline std::string &Args::last()
{
    return *d_strPtrVect.back();
}

inline Args &Args::instance()
{
    return *s_args;
}

#endif
