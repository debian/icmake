#define XERR
#include "optlong.ih"

OptLong::OptLong(PtrVect &ptrVect, unsigned from,                   // 2.cc
                 char const *opts, Option const *longOpts)
:
    d_ptrVect(&ptrVect),
    d_opts(opts),
    d_longOpts(longOpts)
{
    optind = from;
}
