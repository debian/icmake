#define XERR
#include "optlong.ih"

OptLong::OptLong()
:
    d_ptrVect(0),
    d_opts(0),
    d_longOpts(0)
{
    opterr = 0;             // suppress error messages, check '?' returnval.
}
