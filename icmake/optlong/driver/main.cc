//#define XERR
#include "main.ih"
#include "getoptlong.h"

char const *g_actionOptChars{ "+acdefhmNnpsSt:T:uVv" };

option g_actionLongOpts[] =
{
                                            // -q: ignored
                                            // -t: used only by icmake scripts
                                            //     (no long option name)
    // long name            needs arg.  flag    returned char at long name
    //
    { "about",              no_argument, 0,         'a' },
    { "compile",            no_argument, 0,         'c' },
    { "dependencies",       no_argument, 0,         'd' },
    { "execute",            no_argument, 0,         'e' },
    { "force",              no_argument, 0,         'f' },
    { "help",               no_argument, 0,         'h' },
    { "multicomp",          no_argument, 0,         'm' },
    { "no-process",         no_argument, 0,         'N' },
    { "no-version-check",   no_argument, 0,         'n' },
    { "preprocess",         no_argument, 0,         'p' },
    { "source",             no_argument, 0,         's' },
    { "spch",               no_argument, 0,         'S' },
    { "tmpdir",             required_argument, 0,   'T' },
    { "unassemble",         no_argument, 0,         'u' },
    { "verbose",            no_argument, 0,         'V' },
    { "version",            no_argument, 0,         'v' },
    { 0 }
};

int main(int argc, char **argv)
{
    GetoptLong::PtrVect pv{argv, argv + argc};

    GetoptLong gol{ pv, 1, g_actionOptChars, g_actionLongOpts };

    while (true)
    {
        int value = gol.next();

        cout << "value = " << value << "'\n";

        if (value == -1)
        {
            cout << "done\n";
            break;
        }

        if (isprint(value))
            cout << "option: " << (char)value << 
                    ": argument = `" << gol.argument() << 
                    "'\n";
    }
}



