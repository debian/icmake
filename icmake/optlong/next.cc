#define XERR
#include "optlong.ih"

int OptLong::next()
{
    // int getopt_long(int argc, char *argv[],
    //            const char *optstring,
    //            const struct option *longopts, int *longindex);

    int ret = getopt_long(d_ptrVect->size(), &d_ptrVect->front(),
                          d_opts, d_longOpts, 0);

    if (optarg != 0)
        d_argument = optarg;            // global getopt() variable
    else
        d_argument.clear();

    return ret;
}


