#define XERR
#include "optlong.ih"

    // resets s_optLong, keeps opterr and tmp's optind settings
OptLong &OptLong::operator=(OptLong &&tmp)
{
    d_ptrVect = tmp.d_ptrVect;
    d_opts = tmp.d_opts;
    d_longOpts = tmp.d_longOpts;
    d_argument = move(tmp.d_argument);

    return *this;
}
