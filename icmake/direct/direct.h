#ifndef INCLUDED_DIRECT_
#define INCLUDED_DIRECT_

#include "../base/base.h"

class DirectOpts;
class OptBase;

class Direct
{
    DirectOpts &d_directOpts;

    public:
        Direct(OptBase &optBase);
        int run(char const *progName, Base::ExecType type);
};
        
#endif
