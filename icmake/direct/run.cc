#define XERR
#include "direct.ih"

int Direct::run(char const *progName, Base::ExecType type)
{
    string cmd = progName + d_directOpts.argStr();

    Execute execute;
    execute.init(progName, d_directOpts.usage(), d_directOpts.version());

    return execute.run(cmd, type);
}
