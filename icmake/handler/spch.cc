#define XERR
#include "handler.ih"

int Handler::spch() const
{
    return Direct{ d_options.optBase() }.run("icm-spch", Base::DIRECT);
}
