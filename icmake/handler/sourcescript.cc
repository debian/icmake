#define XERR
#include "handler.ih"

int Handler::sourceScript() const
{
    return SourceScript{ d_options.optBase() }.run();
}
