#define XERR
#include "handler.ih"

int Handler::unassemble() const
{
    return Direct{ d_options.optBase() }.run("icm-un", Base::DIRECT);
}
