#define XERR
#include "handler.ih"

int Handler::none() const         // no requested action
{
    xerr("no requested action");
    return 0;
}
