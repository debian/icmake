#define XERR
#include "handler.ih"

int Handler::execute() const
{
    return Direct{ d_options.optBase() }.run("icm-exec", Base::DIRECT);
}
