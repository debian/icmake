//#define XERR
#include "handler.ih"

unordered_map<Options::Action, int (Handler::*)() const> Handler::s_action
{
    { NONE,         &Handler::none          },
    { COMPILE,      &Handler::compile       },
    { DEPENDENCIES, &Handler::dependencies  },
    { EXECUTE,      &Handler::execute       },
    { FORCED,       &Handler::compile       },  // forced set by PreCompOpts
    { MULTICOMP,    &Handler::multicomp     },
    { PREPROCESS,   &Handler::preprocess    },
    { SCRIPT,       &Handler::sourceScript  },
    { SOURCE,       &Handler::sourceScript  },  // handles -s and -t
    { SPCH,         &Handler::spch          },
    { UNASSEMBLE,   &Handler::unassemble    },
};

