#define XERR
#include "handler.ih"

int Handler::compile() const
{
    return IcmComp{ d_options.optBase(), Base::CHILD }.run();  
}
