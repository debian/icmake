#define XERR
#include "handler.ih"

int Handler::dependencies() const
{
    return Direct{ d_options.optBase() }.run("icm-dep", Base::DIRECT);
}
