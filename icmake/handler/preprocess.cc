#define XERR
#include "handler.ih"

int Handler::preprocess() const
{
    IcmPp icmPp{ d_options.optBase(), Base::DIRECT };
    return icmPp.run();
}
