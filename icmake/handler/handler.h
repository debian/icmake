#ifndef INCLUDED_HANDLER_
#define INCLUDED_HANDLER_

#include <iosfwd>
#include <string>
#include <unordered_map>

#include "../base/base.h"

class Options;

class Handler: public Base
{
    Options &d_options;

    unsigned d_idx;

    std::string d_tmpDir;
    std::string d_file2;
    std::string d_preOptions;
    std::string d_execOptions;

    static std::unordered_map<Action, int (Handler::*)() const> s_action;

    public:
        Handler(Options &options);
        int process();

    private:
                            // return values are returned by main(): 0 is OK
        int compile() const;
        int dependencies() const;
        int execute() const;
        int multicomp() const;
        int none() const;
        int preprocess() const;
        int spch() const;
        int sourceScript() const;
        int unassemble() const;
};
        
#endif
