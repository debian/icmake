#define XERR
#include "handler.ih"

int Handler::multicomp() const
{
    return Direct{ d_options.optBase() }.run("icm-multicomp", Base::DIRECT);
}
