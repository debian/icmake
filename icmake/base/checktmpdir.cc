//#define XERR
#include "base.ih"

// static
void Base::checkTmpDir(string &tmpDir)
{
    if (not isWritable(tmpDir)) 
        throw Exception{} << "tmp. dir `" << tmpDir << "' not writable";
}




