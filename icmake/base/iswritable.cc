#define XERR
#include "base.ih"

// static
bool Base::isWritable(string &dirName)
{
    if (isDir(dirName) and access(dirName.c_str(), R_OK | W_OK | X_OK) == 0)
    {
        if (dirName.back() != '/')
            dirName += '/';
        return true;
    }

    return false;
}
