#include "base.ih"

bool Base::isDir(std::string const &path)
{
    return Stat{ path }.isType(Stat::DIRECTORY);
}
