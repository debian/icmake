#include "base.ih"

void Base::setTmpDir(string &tmpDir)
{
    if (tmpDir.empty())
        throw Exception{} << "--tmpdir directory name not specified";

    checkTmpDir(tmpDir);            // tmp dir must be writable

    s_tmpDir = tmpDir;
}
