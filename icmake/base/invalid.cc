#include "base.ih"

// static
void Base::invalid(Action action, char const *arg)
{
    throw Exception{} << "invalid " << actionName(action) <<
                         " argument/option at " << arg;
}
