#include "base.ih"

void Base::show(char const *msg, PtrVect const &ptrVect, bool throws)
{
    if (*msg != 'X')
        cerr << '\n' << msg << '\n';
    else
        cerr << '\n' << (msg + 1) << 
                ". nextIdx = " << OptLong::nextIdx() << '\n';

    for (size_t idx = 0; idx != ptrVect.size(); ++idx)
        cerr << idx << ": `" << ptrVect[idx] << "'\n";
    if (throws)
    {
        cerr << "throws\n";
        throw 0;
    }
}
