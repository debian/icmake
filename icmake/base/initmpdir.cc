//#define XERR
#include "base.ih"

// static
string Base::iniTmpDir()
{
    string ret = "/tmp/";

    if (not isWritable(ret))
    {
        ret = getHome();
        checkTmpDir(ret);
    }

    return ret;
}
