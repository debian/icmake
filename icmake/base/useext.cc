#include "base.ih"

// static
string Base::useExt(string const &filename, char const *ext)
{
    return fs::path{ filename }.replace_extension(ext);
}

