#include "base.ih"


string Base::s_tmpDir = Base::iniTmpDir();

    // the 1st character of the value is the option name,
    // if present at checkAction then other option follow, and
    // all options up to the 
    // Note: Handlder must also have a corresponding entry in its data.cc file
unordered_map<Base::Action, char const *> Base::s_actionName =
{
    { NONE,         "none"          },
    { COMPILE,      "compile"       },
    { DEPENDENCIES, "dependencies"  },
    { EXECUTE,      "execute"       },
    { FORCED,       "forced"        },
    { MULTICOMP,    "multicomp"     },
    { PREPROCESS,   "preprocess"    },
    { SCRIPT,       "script"        },
    { SOURCE,       "source"        },
    { SPCH,         "spch"          },
    { UNASSEMBLE,   "unassemble"    },

    { ICMAKE,       "icmake"        },
};
