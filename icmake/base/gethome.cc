//#define XERR
#include "base.ih"

// static
string Base::getHome()           // with final /
{
    string ret;

    if (char const *cp = getenv("HOME"); cp != 0)
    {
        ret = cp;
        if (ret.back() != '/')
            ret += '/';
    }

    return ret;
}
