#include "main.ih"

int handleException()
try
{
    rethrow_exception(current_exception());
} 
catch(int ret)
{
    return ret;
}
catch (exception const &exc)
{
    cerr << "icmake: " << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cerr << "icmake: unexpected exception\n";
    return 1;
}
