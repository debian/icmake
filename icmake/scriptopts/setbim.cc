#define XERR
#include "scriptopts.ih"

    //----------------------------------------------------------------------
    //     arg1:        a dir?      tmpDir:     d_bimName:      TempStream?
    //----------------------------------------------------------------------
    //  1:  .               unused      tmpDir()    tmpDir()        yes
    //  2: [~]/dir/         yes         [~]/dir     tmpDir()        yes
    //  3: [~]/dir/name     no          [~]/dir                     no
    //----------------------------------------------------------------------

void ScriptOpts::setBim(string arg1)            // 1st argument of -t
{
    if (arg1 == ".")                            // 1st alternative: -t.
    {
        tmpBim(tmpDir());
        return;                                 // a std. temp. name
    }

    if (arg1.front() == '~')                    // locations from $HOME
        arg1.replace(0, 1 + (arg1.find("~/") == 0), getHome());


    if (isDir(arg1))                            // 2nd alternative: 
    {                                           // -tdir/
        setTmpDir(arg1);
        tmpBim(arg1);
        return;
    }

    d_keepBim = true;
    d_bimName = arg1;

    arg1 = fs::path{ arg1 }.parent_path();      // 'string &' needed:
    setTmpDir(arg1);                            // update tmpDir()
}




