#define XERR
#include "scriptopts.ih"

void ScriptOpts::addImAndBim()
{                                   // determine the temp. imname
    char *tmpIm = imName();         // (script w/o #!-line -> TempStream

//FBB    d_compile.push_back(d_Args.add("-T"));
//FBB    d_compile.push_back(d_Args.add(tmpDir()));

    d_compile.push_back(tmpIm);     // set the -c and -e filenames
    char *bim = bimName();

    d_compile.push_back(bim);
    d_execute.push_back(bim);
}
