#define XERR
#include "scriptopts.ih"

// overrides
ScriptOpts::~ScriptOpts()
{
    delete d_imFile;
    delete d_bimFile;

    if (not d_keepBim)
    {
        error_code ec;
        fs::remove(d_bimName, ec);
    }
}
