#ifndef INCLUDED_SCRIPTOPTS_
#define INCLUDED_SCRIPTOPTS_

#include "../stbase/stbase.h"

namespace FBB
{
    class TempStream;
}

class ScriptOpts: public STBase
{
    PtrVect     d_orgArgs;

    FBB::TempStream *d_imFile;
    FBB::TempStream *d_bimFile;

    bool d_keepBim;
    std::string d_bimName;          // the base [dir/]name of the .bim files
    size_t d_idx;

    public:
        ScriptOpts(PtrVect &argv);
        ~ScriptOpts() override;

    private:
                                    // reset may change tmpDir(), puts all
                                    // remaining -t options in argv and sets
                                    // d_bimName
        void reset();               // d_argv is OptsBase's d_argv

        void setOptions();          // set compile/execute options      // .ih
        void addArguments();        // add arguments, including filenames

                                        // may change tmpDir()
        void setBim(std::string arg1);  // 1st -t argument

        void addImAndBim();
        char *imName();
        char *bimName();

        void tmpBim(std::string const &base);

        void addBeyondNonOpt();     // add options beyond the 1st non-opt
        void addCmdlineArgs();

                                    // returns 1 for non-existing files
        static size_t lastModified(std::string const &filename);        // .ih
};
        
#endif
