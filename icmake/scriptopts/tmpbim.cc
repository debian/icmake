#define XERR
#include "scriptopts.ih"

void ScriptOpts::tmpBim(string const &base)
{
    d_bimFile = new TempStream{ base };
    d_bimName = d_bimFile->fileName();
}
