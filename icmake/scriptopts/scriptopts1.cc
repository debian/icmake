#define XERR
#include "scriptopts.ih"

    //  the #! arguments: [0]: the binary
    //                    [1]: 1st line starting at -t
    //                    [2]: the name of the script using -t
    //                    [3...]: command-line argument if specified.

ScriptOpts::ScriptOpts(PtrVect &argv)
:
    STBase(argv),
    d_orgArgs(move(argv)),  // orgArgs grabs argv, argv now cleared 
    d_imFile(0),
    d_bimFile(0)
{
    reset();                // refill (d_)argv

    setOptions();
    addArguments();
    OptLong::setNextIdx(0);
}





