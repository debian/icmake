#define XERR
#include "scriptopts.ih"

    // fills (d_)argv with -t's arguments (d_orgArgs[1])
    // when called the constructor has just cleared (d_)argv

void ScriptOpts::reset()                 // d_argv is OptsBase's d_argv
{
                                                        // -t's argument 
    string tArg{ OptLong::instance().argument() };      // following -t)

    istringstream in{ tArg };           // the 1st scriptline is handled by
    string word;                        // imname()

    in >> word;                         // -t's first argument: tmpspec

                                        // bimBase may remain empty: bimName()
    setBim(word);                       // determines bim-file's name

    while (in >> word)                  // add all remaining '-t ...' words
        d_argv.push_back(d_Args.add(move(word)));
}

