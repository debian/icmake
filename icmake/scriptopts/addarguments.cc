#define XERR
#include "scriptopts.ih"

void ScriptOpts::addArguments()
{
    addImAndBim();
    addBeyondNonOpt();      // add execute options beyond the 1st non option
    addCmdlineArgs();       // add specified command-line options
}
