#define XERR
#include "scriptopts.ih"

void ScriptOpts::addBeyondNonOpt()
{
    if (d_idx == string::npos)      // no options beyond the non-option
        return;

    for (size_t idx = d_idx + 1, end = d_argv.size(); idx != end; ++idx)
        d_execute.push_back(d_argv[idx]);

}
