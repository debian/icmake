#define XERR
#include "scriptopts.ih"

char *ScriptOpts::imName()
{
                                            // construct the .im source
    d_imFile = new TempStream{ tmpDir() };  // always a Temp. File

    ifstream script{ Exception::factory<ifstream>(d_orgArgs[2]) };

    string fstLine;
    if (not getline(script, fstLine))
        throw Exception{} << "Can't read " << d_orgArgs[2];

    size_t pos = fstLine.find_first_not_of(" \t");
    if (fstLine.find("#!", pos) != pos)         // no shebang script
        *d_imFile << fstLine << '\n';           // then keep the 1st line

    *d_imFile << script.rdbuf();           // copy the script's code

    d_imFile->seekg(0);                    // and reset to its 1st line

                                                    // store the filename, 
    return d_Args.add(d_imFile->fileName());   // and return its name
}


