#define XERR
#include "scriptopts.ih"

    // see also setbim.cc
    //----------------------------------------------------------------------
    //     arg1:        a dir?      tmpDir:     d_bimName:      TempStream?
    //----------------------------------------------------------------------
    //  1:  .               unused      tmpDir()    tmpDir()        yes
    //  2: [~]/dir/         yes         [~]/dir     tmpDir()        yes
    //  3: [~]/dir/name     no          [~]/dir                     no
    //----------------------------------------------------------------------

char *ScriptOpts::bimName()
{
            // if the script is older than the bim-file: no need to compile,
            // and STBase::skipCompile returns true

    if (d_keepBim)
        setSkipCompile(lastModified(d_orgArgs[2]) < lastModified(d_bimName));

    return d_Args.add(d_bimName);               // store the bim-filename 
}

