#define XERR
#include "scriptopts.ih"

void ScriptOpts::addCmdlineArgs()
{
    for (size_t idx = 3, end = d_orgArgs.size(); idx != end; ++idx)
        d_execute.push_back(d_orgArgs[idx]);
}

