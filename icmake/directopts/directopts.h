#ifndef INCLUDED_DIRECTOPTS_
#define INCLUDED_DIRECTOPTS_

#include "../optbase/optbase.h"

    // OptBase provides PtrVect d_argv

class DirectOpts: public OptBase
{
    std::string d_argStr;

    public:
        DirectOpts(PtrVect &argv);
        ~DirectOpts() override;

        std::string const &argStr() const;          // inline

    private:
        void arguments();
};
        
inline std::string const &DirectOpts::argStr() const
{
    return d_argStr;
}

#endif
