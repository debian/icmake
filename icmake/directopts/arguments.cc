#define XERR
#include "directopts.ih"

void DirectOpts::arguments()
{
        // add all remaining arguments to d_argStr (returned by args())
    for (
        size_t idx = OptLong::nextIdx(), end = d_argv.size(); 
            idx != end; 
                ++idx
    )
    {
        string argIdx{ d_argv[idx] };
        if (argIdx.find_first_of(" \t") != string::npos)  // embed multi words
            argIdx.insert(0, 1, '\'') += '\'';            // arg in quotes

        (d_argStr += ' ') += argIdx;
    }
}

