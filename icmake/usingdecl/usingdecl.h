#ifndef INCLUDED_USINGDECL_
#define INCLUDED_USINGDECL_

#include <vector>
#include <string>

struct UsingDecl
{
    using PtrVect = std::vector<char *>;
    using StrVect = std::vector<std::string>;
};
        
#endif
