#define XERR
#include "optbase.ih"

void OptBase::remaining(char const *caller) const
{
    cerr << "(OptBase::remaining) " << caller << ":\n"
            "   ";

    for (int idx = 0; idx != optind; ++idx)     // 'int' because of optind
        cerr << d_argv[idx] << ' ';
    cerr << "][ ";
    for (size_t idx = optind; idx != d_argv.size(); ++idx)
        cerr << d_argv[idx] << ' ';
    cerr << '\n';
}
