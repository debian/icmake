#ifndef INCLUDED_OPTBASE_
#define INCLUDED_OPTBASE_

#include <getopt.h>

#include <cstddef>

#include "../base/base.h"

struct OptBase: public Base
{
    using LongOpt = struct option;              // cf. getopt_long

    private:
        bool d_usage;           // if true -h was specified
        bool d_version;         // if true -v was specified
    
        StrVect d_plainArgs;

    protected:
        PtrVect &d_argv;            // freely modifiable by (and provided by)
                                    //  the derived class
    public:
        OptBase(PtrVect &argv);                 //              1.cc
        virtual ~OptBase();

        size_t tailIdx() const;                 // inline 

        void setUsage();                        // inline
        void setVersion();                      // inline
        bool usage() const;                     // inline
        bool version() const;                   // inline

        StrVect const &plainArgs() const;       // inline

                                                // may redefine d_plainArgs
        std::string setPlain(size_t idx, std::string const &str);

        void remaining(char const *caller) const;   // remaining opts/args

    protected:
        void setPlainArgs();
};
        
inline size_t OptBase::tailIdx() const
{
    return optind;
}

inline void OptBase::setUsage()
{
    d_usage = true;
}

inline void OptBase::setVersion()
{
    d_version = true;
}

inline bool OptBase::usage() const
{
    return d_usage;
}

inline bool OptBase::version() const
{
    return d_version;
}

inline OptBase::StrVect const &OptBase::plainArgs() const
{
    return d_plainArgs;
}

#endif


