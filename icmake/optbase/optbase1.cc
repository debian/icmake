#define XERR
#include "optbase.ih"

    // cf: https://stackoverflow.com/questions/19940100/
    //              is-there-a-way-to-reset-getopt-for-non-global-use
    //
    //    optind = 0;   // resets getopt_long, done by OptBase, in which
    //
    // case argv[optind] is ignored, and remaining argv values specifiy
    // options/arguments processed by getopt_long

    // optind specifies the index of the next argument processed by
    // getopt_long. But when reset to 0 argv[0] must point to a value which
    // is ignored by getopt_long, whereafter remaining options and arguments
    // follow.

OptBase::OptBase(PtrVect &argv)
:
    d_usage(false),
    d_version(false),
    d_argv(argv)
{}




