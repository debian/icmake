#define XERR
#include "optbase.ih"

string OptBase::setPlain(size_t idx, string const &str)
{
    if (d_plainArgs.size() <= idx)
        d_plainArgs.resize(idx + 1);        // room for the new string

    string ret{ move(d_plainArgs[idx]) };   // return the orig. value
    d_plainArgs[idx] = str;                 // and modify [idx] to str

    return ret;
}
