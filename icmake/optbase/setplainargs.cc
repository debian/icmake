#define XERR
#include "optbase.ih"

void OptBase::setPlainArgs()
{
    for (size_t idx = tailIdx(), end = d_argv.size(); idx != end; ++idx)
    {
        if (*d_argv[idx] != '-')                    // not an option
            d_plainArgs.push_back(d_argv[idx]);
    }
}
