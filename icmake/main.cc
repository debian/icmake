#define XERR
#include "main.ih"

int main(int argc, char **argv)
try
{
    if (argc == 1)                          // no args: provide usage
    {
        Options::usage(*argv);
        return 1;
    }

    Options options(argc, argv);            // prepares the options for 
                                            // processing by sub-programs
    Handler handler{ options };
    return handler.process();
}
catch (...)
{
    return handleException();
}
