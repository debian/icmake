#define XERR
#include "sourceopts.ih"

SourceOpts::SourceOpts(PtrVect &argv)
:
    STBase(argv),
    d_bim(0)
{
                                        // prefill d_compile/d_execute
    size_t idx = addOptions(OptLong::nextIdx());

    if (idx == string::npos)
        throw Exception{} << "no .im file specified";

    addImAndBim(idx);               // add the .im file and the .bim file
    addBimArgs(idx + 1);            // add arguments for the bim file

    OptLong::setNextIdx(0);

//show("sourceopts/sourceopts1  compile opts: ", d_compile, false);
}



