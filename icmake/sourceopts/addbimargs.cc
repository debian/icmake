#define XERR
#include "sourceopts.ih"

void SourceOpts::addBimArgs(size_t fromIdx)
{
    for ( ; fromIdx != d_argv.size(); ++fromIdx)    // add the 'execute' args
        d_execute.push_back(d_argv[fromIdx]);

}
