#ifndef INCLUDED_SOURCEOPTS_
#define INCLUDED_SOURCEOPTS_

#include "../stbase/stbase.h"

namespace FBB
{
    class TempStream;
}

class SourceOpts:  public STBase
{
    FBB::TempStream *d_bim;

    public:
        SourceOpts(PtrVect &argv);                  // 1.cc
        ~SourceOpts() override;
    
    private:
        void addImAndBim(size_t idx);
        void addBimArgs(size_t fromIdx);    // with -t: adds cmd line args

};

#endif


