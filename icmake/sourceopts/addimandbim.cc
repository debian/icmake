#define XERR
#include "sourceopts.ih"

void SourceOpts::addImAndBim(size_t idx)
{
    char *imFile = d_argv[idx];             // the file to compile

    OptLong::setNextIdx(d_compile.size());

    d_compile.push_back(imFile);            // add as 1st non option

    d_bim = new TempStream{ tmpDir() };
    char *bimFile = d_Args.add(d_bim->fileName());  // get the .bim filename

    d_compile.push_back(bimFile);      // compile to .bim
    d_execute.push_back(bimFile);      // and execute .bim
}
