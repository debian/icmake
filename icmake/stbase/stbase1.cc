#define XERR
#include "stbase.ih"

STBase::STBase(PtrVect &argv)
:
    OptBase(argv),
    d_idx(string::npos),
    d_skipCompile(false),           // by default: compile
    d_Args(Args::instance()),
    d_compile(1, d_Args.add("-c")), 
    d_execute(1, d_Args.add("-e"))
{}
