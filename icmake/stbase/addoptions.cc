#define XERR
#include "stbase.ih"

size_t STBase::addOptions(size_t idx)
{
    size_t size = d_argv.size();
    OptLong::setNextIdx(idx);         // d_argv contains options
    
    d_idx = idx;

    while (
        d_idx < size
        and
        (
            hvOption() 
            or 
            dOption() 
            or 
            eOption()
        )
    )
        ;
                                     // maybe -t does not specify an im file
    return (d_idx != size) ? d_idx : string::npos;
}
