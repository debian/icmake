#define XERR
#include "stbase.ih"

namespace
{
    vector<string> const options{ "--help", "--version", "-h", "-v"};
};

bool STBase::hvOption()
{
    if (find(options.begin(), options.end(), d_argv[d_idx]) != options.end())
    {
        d_compile.push_back(d_argv[d_idx++]);
        return true;
    }

    return false;
}
