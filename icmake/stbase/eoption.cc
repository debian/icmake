#define XERR
#include "stbase.ih"

bool STBase::eOption()
{
    string opt = d_argv[d_idx];        // get the current d_argv element

    if (opt.find("-e") == 0)          // short option
    {
        if (opt.length() <= 2)           
            d_execute.push_back(d_argv[d_idx]);
        else                        // option value is appended
        {
            d_argv[d_idx][1] = '-';    // option starts at index pos. 1
            d_execute.push_back(d_argv[d_idx] + 1);
        }
        ++d_idx;
        return true;
    }

    if (opt == "--execute")                  // long option, separate value
    {
        d_execute.push_back(d_argv[d_idx++]);
        return true;
    }

    if (opt.find("execute=") == 0)  // long option=value
    {                               // set 'value as option
        d_execute.push_back(d_argv[d_idx++] + size("execute=") - 1);
        return true;
    }

    return false;                   // no -e/--execute option        
}
