#ifndef INCLUDED_STBASE_
#define INCLUDED_STBASE_

#include "../optbase/optbase.h"

#include "../precompopts/precompopts.h"
#include "../directopts/directopts.h"

class Args;

class STBase: public OptBase
{
    size_t d_idx;           // OptLong's nextIdx

    bool d_skipCompile;             // by default false (compilation is 
                                    // performed) but true at -tdir if 
                                    // scriptname younger scriptname.bim
    protected:
        Args   &d_Args;

        PtrVect d_compile;
        PtrVect d_execute;

    public:
        STBase(PtrVect &argv);
        ~STBase() override;

        PreCompOpts compileOpts();                  // inline (2x)
        DirectOpts  executeOpts();

        bool script() const;                        // inline (3x)
        bool skipCompile() const;
        void setSkipCompile(bool value);

    protected:
                                // returns the 1st non-option idx or str::npos
        size_t addOptions(size_t from); // from: start position in d_argv

    private:
        bool hvOption();
        bool dOption();
        bool eOption();
};

inline bool STBase::skipCompile() const
{
    return d_skipCompile;
}

inline void STBase::setSkipCompile(bool value)
{
    d_skipCompile = value;
}

inline PreCompOpts STBase::compileOpts()
{
    return PreCompOpts{ d_compile, "d:hv", Base::COMPILE };
}

#endif
