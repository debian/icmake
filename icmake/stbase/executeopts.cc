#define XERR
#include "stbase.ih"

DirectOpts STBase::executeOpts()
{
//    show("STBase/ executeOpts", d_execute, false);

    OptLong::setNextIdx(1);         // skip -e
    return DirectOpts{ d_execute };
}

