#define XERR
#include "stbase.ih"

bool STBase::dOption()
{
    string opt = d_argv[d_idx];        // get the current d_argv element

    if (opt.find("-d") == 0)          // short option
    {
        d_compile.push_back(d_argv[d_idx++]);    // store option

        if (opt.length() == 2)              // only short option, value next
            d_compile.push_back(d_argv[d_idx++]);
        return true;
    }

    if (opt == "--define")                  // long option, separate value
    {
                                            // store option + separate value
        d_compile.push_back(d_argv[d_idx++]);
        d_compile.push_back(d_argv[d_idx++]);
        return true;
    }

    if (opt.find("--define=") == 0)             // long option=value
    {
        d_compile.push_back(d_argv[d_idx++]);
        return true;
    }

    return false;                   // no -d/--define option        
}

