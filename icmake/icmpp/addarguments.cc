#define XERR
#include "icmpp.ih"

void IcmPp::addArguments(string &cmd) const
{                                                       // add the plain args
    auto const &plainArgs = d_preCompOpts.plainArgs();  // + maybe [first].pim

    char const *arg1 = &plainArgs.front().front();

    (cmd += ' ') += arg1;
                                    // only 'arg[.xxx]' uses 2nd: 'arg.pim'
    if (d_preCompOpts.plainArgs().size() == 1)
        (cmd += ' ') += Base::useExt(arg1, ".pim");
    else
    {
        for (size_t idx = 1, end = plainArgs.size();  idx != end; ++idx)
            (cmd += ' ') += plainArgs[idx];
    }
}
