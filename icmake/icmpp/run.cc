#define XERR
#include "icmpp.ih"

int IcmPp::run()
{
    Execute execute;

    execute.init("icm_pp", d_preCompOpts.usage(), d_preCompOpts.version());

    string cmd{ "icm-pp" + d_preCompOpts.defineStr()};

    if (not d_preCompOpts.plainArgs().empty())  // add non-option arguments
        addArguments(cmd);

    return execute.run(cmd, d_type);
}







