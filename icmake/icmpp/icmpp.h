#ifndef INCLUDED_ICMPP_
#define INCLUDED_ICMPP_

#include <iosfwd>

#include "../base/base.h"

class PreCompOpts;
class OptBase;

class IcmPp
{
    PreCompOpts &d_preCompOpts;
    Base::ExecType d_type;              // Execute as CHILD or DIRECT

    public:
        IcmPp(OptBase &optBase, Base::ExecType type);
        int run();

    private:
        void addArguments(std::string &cmd) const;
};
        
#endif
