#define XERR
#include "icmpp.ih"

IcmPp::IcmPp(OptBase &optBase, Base::ExecType type)
:
    d_preCompOpts(dynamic_cast<PreCompOpts &>(optBase)),
    d_type(type)
{}
