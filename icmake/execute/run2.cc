#define XERR
#include "execute.ih"

bool Execute::run(string const &cmd)
{
    Generic const &generic = Generic::instance();

        // only actions supporting version checks can suppress the 
        // version check.
    size_t pos = cmd.find_first_of(' ');

    if (pos == string::npos)
        d_exec.setCommand(LIBDIR "/" + cmd + generic.noVersionCheck());
    else
        d_exec.setCommand(LIBDIR "/" + cmd.substr(0, pos) +
                                       generic.noVersionCheck() +
                                       cmd.substr(pos));

    if (generic.verbose())
        cout << "calling `" << d_exec.str() << "'\n";

    return not generic.noProcess();
}
