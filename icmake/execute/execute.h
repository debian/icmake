#ifndef INCLUDED_PROCESS_
#define INCLUDED_PROCESS_

#include <iosfwd>

#ifdef fbb
    #include <bobcat/process>
#else
    #include "../../tmp/build/process/process"
#endif

#include "../base/base.h"

    // executes a child process: DIRECT replaces the current process
    //                           CHILD  runs as a child process
    //          the sub-processes are in LIBDIR (/usr/libexec/icmake)

class Execute
{
    FBB::Process d_exec;

    public:
        Execute() = default;

        int run(std::string const &cmd, Base::ExecType type);       // 1.cc

        void init(std::string progName, bool usage, bool version);

    private:            
        int direct(std::string const &cmd);    // calls LIBDIR/cmd 
                                                // replacing the current 
                                                // process
        int child(std::string const &cmd);      // same, runs the process as
                                                // a child-process

                                                // handles d_verbose,
        bool run(std::string const &cmd);       // returns d_process    2.cc
};
        
#endif
