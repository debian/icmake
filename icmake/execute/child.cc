#define XERR
#include "execute.ih"

int Execute::child(string const &cmd)
{
    if (not run(cmd))
        return 0;

    d_exec.start(Process::NONE);

    return d_exec.waitForChild();
}
