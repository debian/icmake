#define XERR
#include "execute.ih"

int Execute::run(string const &cmd, Base::ExecType type)
{
    return type == Base::DIRECT ? direct(cmd) : child(cmd);
}
