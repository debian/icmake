#define XERR
#include "execute.ih"

void Execute::init(string progName, bool usage, bool version)
{
    if (usage)
        direct(progName += " -h");

    if (version)
        direct(progName += " -v");
}
