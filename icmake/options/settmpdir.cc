//#define XERR
#include "options.ih"

bool Options::setTmpDir()
{
    string arg = OptLong::instance().argument();
    Base::setTmpDir(arg);

    return true;
}
