#define XERR
#include "options.ih"

    // determine the requested action from the initial option(s)/argument(s)
void Options::arguments()
{
    // initially optind == 1, so it skips the program name
    // before nested getopt_long calls optind is set to 0 to reset 
    // getopt_long, so interpreting the initial '+' of action arguments ends
    // cf: https://stackoverflow.com/questions/19940100/
    //              is-there-a-way-to-reset-getopt-for-non-global-use

    separateActionOpts();       // e.g., -dVVV becomes -d -VVV,
                                // but not for -txx, which becomes -t xx


    OptLong &opts = 
                OptLong::init(d_argv, 1, s_actionOptChars, s_actionLongOpts);


    int key;
    do                          // argv must point to modifiable chars:
    {                           // 'char *const *'
        key = opts.next();

        if (key == '?')
            OptBase::invalid(ICMAKE, d_argv[opts.nextIdx() - 1]);
    }
    while ((this->*s_action[key])());   // calls a function from s_action
        // when the do-while loop ends OptLong's nextIdx() is just beyond
        // the index of the option ending the while loop.
}






