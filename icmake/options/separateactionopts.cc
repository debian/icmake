#define XERR
#include "options.ih"

void Options::separateActionOpts()
{
    size_t idx = 1;                     // start beyond icmake's binary
    while (idx != d_argv.size())        // process the arguments
    {
        string arg = d_argv[idx];      // inspected argument
        size_t pos;

        if (next(&pos, arg))
        {
            ++idx;
            continue;
        }
                                // argument(s) following -t are kept and are 
                                // handled by sourceopts2.cc (see script.cc)  
        if (arg.find("-t") == 0)
            return;

        if (arg[pos + 1] == 0)          // if no more option chars:
            break;                      // accept the options as-is

        split(idx, pos);                // split after the action option
        return;
    }
}
