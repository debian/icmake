#define XERR
#include "options.ih"

    // just received option -d
bool Options::directOpts(Action action)
{
    checkAction(action);

    d_optBase = new DirectOpts{ d_argv };

    return false;           // no further actions wrt processing options
}
