#define XERR
#include "options.ih"

namespace
{                                               // supporting no-versioncheck
    string check{ Base::EXECUTE, Base::UNASSEMBLE };    
}

void Options::checkAction(Action action)
{
    if (d_action != NONE)
        throw Exception{} << 
                    "cannot specify multiple actions (2nd: --" << 
                    actionName(action);
    
                                    // version checks only for these actions:
    Generic::instance().supportNoVersionCheck( check.contains(action) );

    d_action = action;
}
