//#define XERR
#include "options.ih"

    // initial +: processing stops at the 1st non-option argument
    //
char const *Options::s_actionOptChars{ "+acdefhmNnpsSt:T:uVv" };

option Options::s_actionLongOpts[] =
{
                                            // -q: ignored
                                            // -t: used only by icmake scripts
                                            //     (no long option name)
    // long name            needs arg.  flag    returned char at long name
    //
    { "about",              no_argument, 0,         'a' },
    { "compile",            no_argument, 0,         'c' },
    { "dependencies",       no_argument, 0,         'd' },
    { "execute",            no_argument, 0,         'e' },
    { "force",              no_argument, 0,         'f' },
    { "help",               no_argument, 0,         'h' },
    { "multicomp",          no_argument, 0,         'm' },
    { "no-process",         no_argument, 0,         'N' },
    { "no-version-check",   no_argument, 0,         'n' },
    { "preprocess",         no_argument, 0,         'p' },
    { "source",             no_argument, 0,         's' },
    { "spch",               no_argument, 0,         'S' },
    { "tmpdir",             required_argument, 0,   'T' },
    { "unassemble",         no_argument, 0,         'u' },
    { "verbose",            no_argument, 0,         'V' },
    { "version",            no_argument, 0,         'v' },
    { 0 }
};

    // these options are action options (ending arguments()) and must be at
    // the end of single char options. If not, its trailing chars become a
    // separate option. E.g., 
    //      -NpVVV nogo -> -Np -VVV nogo (-tccc is kept)
string Options::s_returnOpts{ "cdefmpstSu" };

    // these functions assign d_optBase when returning false
    // the keys have values returned by OptLong::next. E.g., 'c' == 99
unordered_map<int, Options::ActionFun> Options::s_action =
{
    {  'a',     &Options::about             },  // ends in throw 0
    {  'c',     &Options::compile           },  // returns at -c
    {  'd',     &Options::dependencies      },  // returns at -d
    {  'e',     &Options::execute           },  // returns at -e
    {  'f',     &Options::forced            },  // returns at -f
    {  'h',     &Options::usage             },  // ends in throw 0
    {  'm',     &Options::multicomp         },  // returns at -m
    {  'N',     &Options::setNoProcess      },  // continues (-> Generic)
    {  'n',     &Options::setNoVersionCheck },  // continues (-> Generic)
    {  'p',     &Options::preProcess        },  // returns at -p
    {  's',     &Options::source            },  // returns at -s
    {  'S',     &Options::spch              },  // returns at -S
    {  't',     &Options::script            },  // returns at -t
    {  'T',     &Options::setTmpDir         },  // continues
    {  'V',     &Options::setVerbose        },  // continues (-> Generic)
    {  'u',     &Options::unassemble        },  // returns at -u
    {  'v',     &Options::version           },  // ends in throw 0
};



