#define XERR
#include "options.ih"

Options::Options(char **argv, unsigned argc)
:
//    d_args{ argv, argv + argc },
    d_argv{ argv, argv + argc },
    d_scriptBim("."),
    d_action(NONE),
    d_optBase(0)
{
    opterr = 0;                 // return ? on errors
//show("options/options1", d_argv);
}
