#define XERR
#include "options.ih"

        // split d_argv[idx] beyond pos. At 'pos' is the action option

void Options::split(size_t idx, size_t pos)
{                                           // duplicate d_args[idx]
//    d_args.insert(d_args.begin() + idx + 1, d_args[idx]);    

    d_argv.insert(d_argv.begin() + idx + 1, 
                  Args::instance().add(string{ d_argv[idx] }) );

    string &last = Args::instance().last();   // the just added string
    last.erase(1, pos);                 // rm chars 1..the action option
    
                                        // in the org ptr: rm options beyond 
    d_argv[idx][pos + 1] = 0;           // the action
}








