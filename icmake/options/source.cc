#define XERR
#include "options.ih"

bool Options::source()
{
    checkAction(SOURCE);

    d_optBase = new SourceOpts{ d_argv };

    return false;           // no further actions wrt processing options
}
