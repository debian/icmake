//#define XERR
#include "options.ih"

    // received option -p/-c/-f
bool Options::compOpts(Action action, char const *options)
{
    checkAction(action);

    d_optBase = new PreCompOpts{ d_argv, options, action };

    return false;           // no further actions wrt processing options

}
