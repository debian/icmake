
#define XERR
#include "options.ih"

// (see also handler/script)
//
// assuming an (executable) icmake script, named, e.g., icscript, it's first
// line must start with 
//
//      #!/usr/bin/icmake -t t-option argument (... any arguments ...)
//
// icmake then receives all the arguments at the 1st line, and a final 
// argument which is icscript's path.
//
// the options are interpreted by this function, and icscript's content beyond
// the 1st line are processed by as .im, or .pim file and the compiled file is
// executed.
//
// See also 
//      https://stackoverflow.com/questions/3009192/how-does-the-shebang-work

// When using a script icmake receives at least two arguments: the 1st
// argument (argv[1]) contains everything on the 1st line following the
// #!/usr/bin/icmake specificaition, the 2nd argument (argv[2]) is the name of
// the script, and any additional arguments (up to argc: argv[3] ...) are the
// command-line arguments which were specified on the command-line when the
// script was called. 

// Options/arguments specified on the script's first line after
// -t<arg> are forwarded as options/arguments of icmake -s, and icmake -t...'s
// second argument is compiled/execute as icmake script.
// E.g., whe 'script' specifies the following 1st line:
//      #!/usr/bin/icmake -t. -d dep : a -b c
// then calling 
//      script one -two
// compiles 'script' with option '-d dep', while 'script' receives the
// arguments (arg[0] as illustration)
//      /tmp/234TU a -b -c one -two


// When using the command-line call specifying -t then the arguments following
// -t are handled separately, as with other arguments.


bool Options::script()
{
    checkAction(SCRIPT);

    d_optBase = new ScriptOpts{ d_argv };

    return false;
}












