#define XERR
#include "options.ih"

Options::Options(unsigned argc, char **argv)
:
    Options(argv, argc)         // ensure construction -> destruction
{
    arguments();                // determine the action to perform
}
