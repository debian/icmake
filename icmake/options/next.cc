//#define XERR
#include "options.ih"

// static 
bool Options::next(size_t *pos, string const &arg)
{
    return
        arg.front() != '-'          // it's not an option
        or                          // or
        arg[1] == '-'               // it's a long option 
        or                          // or it's not an action option:
        (*pos = arg.find_first_of(s_returnOpts)) == string::npos;
}
