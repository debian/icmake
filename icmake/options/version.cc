#include "options.ih"

bool Options::version()
{
    string progname{ d_argv.front() };

    size_t idx = progname.rfind('/');

    if (idx == string::npos)
        idx = 0;
    else
        ++idx;

    cout << progname.substr(idx) << " V" VERSION "\n";

    throw 0;
}
        
