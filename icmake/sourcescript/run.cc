#define XERR
#include "sourcescript.ih"

int SourceScript::run() const
{
    if (int ret = compile(); ret != 0)
        return ret;

    Generic::instance().supportNoVersionCheck(true);

    DirectOpts executeOpts = d_stBase.executeOpts();

    return Direct{ executeOpts }.run("icm-exec", Base::CHILD);
}

