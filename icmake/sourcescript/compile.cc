#define XERR
#include "sourcescript.ih"

int SourceScript::compile() const
{                                       // maybe compile (as a child process)
    PreCompOpts preCompOpts = d_stBase.compileOpts();

    return IcmComp{ preCompOpts, Base::CHILD, d_stBase.skipCompile() }.run();
}

