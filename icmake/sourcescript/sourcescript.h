#ifndef INCLUDED_SOURCESCRIPT_
#define INCLUDED_SOURCESCRIPT_

class STBase;
class OptBase;

class SourceScript
{
    STBase &d_stBase;

    public:
        SourceScript(OptBase &optBase);

        int run() const;

    private:
        int compile() const;
};
        
#endif
