#define XERR
#include "precompopts.ih"

// static
string PreCompOpts::catStr(StrVect const &strVect)
{
    string ret;
    for (string const &opt: strVect)
        (ret += ' ') += opt;
    return ret;
}
