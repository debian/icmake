#define XERR
#include "precompopts.ih"

namespace
{                                                         // always available
    OptBase::LongOpt longOpts[] =                         // or only for 
    {                                                     // specified options
        { "execute",            required_argument, 0,   'e' },  // -s, -t
        { "preprocess",         no_argument, 0,         'P' },  // -c, -f

        { "define",             required_argument, 0,   'd' },
        { "help",               no_argument, 0,         'h' },
        { "version",            no_argument, 0,         'v' },
        { 0 }
    };
}
        // options -s and -t: options are interpreted until the 1st non-option
        // (-t's own option is handled separately)
        // 
void PreCompOpts::arguments(char const *options)
{
        // nextIdx() returns the index after the -s option or at -c/-p,
        // (which is 1)
    OptLong &opts = OptLong::init(d_argv, OptLong::nextIdx(),
                                  options, longOpts);
    while (true)
    {
        int idx = opts.next();

        switch (idx)
        {
            case 'P':
                require("--preprocessed", string{ COMPILE, FORCED });

                if (not d_define.empty())           // -d already encountered
                    mutuallyExclusive();            // -d and -P: throws

                d_preProcessed = true;
            continue;                

            case 'h':
                setUsage();                 // no further actions required
            return;

            case 'v':
                setVersion();               // no further actions required
            return;

            case 'd':
                if (d_preProcessed)         // -P already encountered
                    mutuallyExclusive();

                d_define.push_back("-d ");
                d_define.back() += opts.argument();
            continue;

            case 'e':
                require("--execute", string{ SOURCE, SCRIPT });
                d_eOptions.push_back("-e ");
                d_eOptions.back() += opts.argument();
            continue;

            case '?':
                invalid(d_action, d_argv[opts.nextIdx() - 1]);
            return;                         // not reached

            default:
            throw Exception{} << __FILE__ ": unexpected getopt val " << idx;

            case -1:        // getopt_long's done.
                setPlainArgs();
            return;
        }
    }
}


