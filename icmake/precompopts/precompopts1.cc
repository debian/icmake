#define XERR
#include "precompopts.ih"


    // optind specifies the index of the *next* argument processed by
    // getopt_long. When reset to 0 argv[0] must point to a value which
    // is ignored by getopt_long, whereafter remaining options and arguments
    // follow.

PreCompOpts::PreCompOpts(PtrVect &argv, char const *options, Action action)
:
    OptBase(argv),          // optbase skips the org. argv[0]
    d_action(action),       // and sets optind to 0. Following arg[0] the
    d_preProcessed(false)   // remaining options/arguments follow
{
    arguments(options);
}
