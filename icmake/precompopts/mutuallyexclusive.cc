#define XERR
#include "precompopts.ih"

//static
void PreCompOpts::mutuallyExclusive()
{
    throw Exception{} << 
                    "--compile: options -d and -P are mutually exclusive";
}
