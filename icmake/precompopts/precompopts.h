#ifndef INCLUDED_PRECOMPOPTS_
#define INCLUDED_PRECOMPOPTS_

#include "../optbase/optbase.h"

    // OptBase provides PtrVect d_argv

class PreCompOpts: public OptBase
{
    char const *d_options;

    Action d_action;
    bool    d_preProcessed;

    StrVect d_define;
    StrVect d_eOptions;

    std::string d_scriptArg;                // -t's argument value

    public:
        PreCompOpts(PtrVect &argv, char const *options, Action type);  // 1.cc

        ~PreCompOpts() override;

        bool preProcessed() const;              // inline
        bool forced() const;                    // inline

        StrVect const &define() const;      // inline
        StrVect const &eOptions() const;    // inline

                                            // returns all d/e options 
                                            // each prefixed by ' '
        std::string defineStr() const;      // inline
        std::string eOptsStr() const;       // inline

        std::string const &scriptArg() const;       // inline
        void setScriptArg(std::string const &arg);  // inline

    private:
        void arguments(char const *options);

        void require(char const *option,                // may throw
                     std::string const &actions) const;

        static void mutuallyExclusive();                // throws
        static std::string catStr(StrVect const &strVect);
};

inline bool PreCompOpts::preProcessed() const
{
    return d_preProcessed;
}

inline bool PreCompOpts::forced() const
{
    return d_action == FORCED;
}

inline void PreCompOpts::setScriptArg(std::string const &arg)
{
    d_scriptArg = arg;
}

inline std::string const &PreCompOpts::scriptArg() const
{
    return d_scriptArg;
}

inline PreCompOpts::StrVect const &PreCompOpts::eOptions() const
{
    return d_eOptions;
}

inline PreCompOpts::StrVect const &PreCompOpts::define() const
{
    return d_define;
}

inline std::string PreCompOpts::eOptsStr() const
{
    return catStr(d_eOptions);
}

inline std::string PreCompOpts::defineStr() const
{
    return catStr(d_define);
}


#endif
