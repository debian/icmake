#define XERR
#include "precompopts.ih"

void PreCompOpts::require(char const *option, string const &actions) const
{
    if (actions.find(d_action) == string::npos)
        throw Exception{} << "option " << option <<
                             ": not available for action " << 
                             actionName(d_action);
}
