#ifndef INCLUDED_GENERIC_
#define INCLUDED_GENERIC_

#include <string>

    // Singleton containing std. options like verbose/don't process

class Generic
{
    bool d_verbose;
    bool d_noProcess;
                                // -n option suppresses version checks
                                // -n options are recognized by actions
                                // EXECUTE, SCRIPT, and UNASSEMBLE. SOURCE
                                // may activate it when executing a .bim file
    bool d_noVersionCheck;      // 
    bool d_supportNoVersionCheck;   // set by actions allowing versionchecks,
                                    // inspected by Execute::run
    public:
        static Generic &instance();

        Generic(Generic const &other) = delete;

        void setVerbose();
        bool verbose() const;

        void setNoProcess();
        bool noProcess() const;

        void setNoVersionCheck();           // called at Option -n 

        void supportNoVersionCheck(bool yesNo); // if true Option's -n option 
                                                // is used when specified

        std::string noVersionCheck() const; // "" or "-n"

    private:
        Generic();
        

};

inline void Generic::setVerbose()
{
    d_verbose = true;
}

inline bool Generic::verbose() const
{
    return d_verbose;
}

inline bool Generic::noProcess() const
{
    return d_noProcess;
}

inline std::string Generic::noVersionCheck() const
{
    return d_supportNoVersionCheck and d_noVersionCheck ? " -n" : "";
}

inline void Generic::setNoVersionCheck()
{
    d_noVersionCheck = true;
}

inline void Generic::supportNoVersionCheck(bool yesNo)
{
    d_supportNoVersionCheck = yesNo;
}

#endif
