#include "generic.ih"

namespace
{
    unique_ptr<Generic> ptr;
}

// static
Generic &Generic::instance()
{
    if (not ptr)
        ptr.reset(new Generic);

    return *ptr;
}
