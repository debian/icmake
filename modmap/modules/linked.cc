#define XERR
#include "modules.ih"

    // called by requiredExternal

    // 'extModgcm'  is, e.g., 'extern/Mod1.gcm'
    // scan's modvect[unknownIdx] is, e.g., 'ModName.gcm'

bool Modules::linked(Path const &extModgcm, size_t unknownIdx)
{
                                    // extModgcm is the searched Module ?
    if (extModgcm.filename().string() 
            != gcmName(d_modVect[unknownIdx].modName())
    )
        return false;               // no: done here

                                    // yes: add it (+ required modules)

    d_modVect[unknownIdx].setExtern();   // not UNKNOWN anymore: now EXTERN
    d_unknowns.erase(unknownIdx);

    connect(extModgcm);             // extModgcm may depend on its own modules
    return true;
}
