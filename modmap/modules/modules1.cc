#define XERR
#include "modules.ih"

//  verbose     dependencies
//      T           T           compile
//      T           F           compile
//      F           T           don't compile
//      F           T           compile

Modules::Modules()
:
    d_options(Options::instance()),
    d_cwd(fs::current_path()),
    d_scan(d_modVect, d_modNameIdx),
    d_dependencies(d_idx, d_modVect, d_modNameIdx)
{
                                            // if already exist, false but
    fs::create_directory("gcm.cache", s_errorCode);     // error_code == 0

    fs::create_directories("tmp/o", s_errorCode);       // same

    createSymlink("/usr", "gcm.cache/usr");
}




