#define XERR
#include "modules.ih"

    // called by fill.cc / inspectsubdirs.cc

void Modules::inspect(Classes::Info const &info)
{
    imsg << "Inspecting " << info.subdir << '/' << endl;

    setDir(info.subdir);                        // the source file's dir

    if (not fs::exists("gcm.cache"))            // ln -sf to main's gcm.cache
    {
        imsg << "   ";
        createSymlink("../gcm.cache", "gcm.cache");
    }

    d_scan.setNr(info.nr);

    for (auto const &entry: fs::directory_iterator{ "." })
    {
        if (
            fs::path const &src = entry.path();             // a requested 
            src.extension() == d_options.extension()        // source file
        )
            d_scan.process(info.subdir, src.string().substr(2));
    }

    setDir(d_cwd);
}





