#define XERR
#include "modules.ih"

    // called by linked

    // 'extModgcm'  is, e.g., 'extern/Mod1.gcm'
    // which is required by the current project

void Modules::connect(Path const &extModgcm)
{
    imsg << "       adding external module " << extModgcm << endl;
    
    if (
        string cacheName = "gcm.cache/" + extModgcm.filename().string();
        not fs::exists(cacheName)
    )
    {
        symlink("../" + extModgcm.string(), cacheName);
        addRequiredBy(extModgcm);       // recursively add modules required
                                        // by extModGcm
    }
}
