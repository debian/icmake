#define XERR
#include "modules.ih"

    // called by addrequiredby.cc

void Modules::addExtern(Path const &extModgcm)
{
    string cacheName{ "gcm.cache/" + extModgcm.filename().string() };

    if (fs::exists(cacheName))                  // already linked: done here
        return;

    string modName = extModgcm.stem().string();
    for (char &ch: modName)
    {
        if (ch == '-')
            ch = ':';
    }
                                                // was: d_external ??
    d_modVect.push_back(ModData{ modName, ModData::EXTERN, 
                                                                extModgcm });
    
    if (symlink("../" + extModgcm.string(), cacheName)) // new symlink:
        addRequiredBy(extModgcm);               // also inspect extModgcm's
                                                // requirements
}
