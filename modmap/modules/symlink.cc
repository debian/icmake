#define XERR
#include "modules.ih"

    // called by addextern, connect

// static
bool Modules::symlink(string const &destination, string const &cacheName)
{
    fs::create_symlink(destination, cacheName, s_errorCode);
    
    if (not s_errorCode)
        return true;

    emsg << "cacheName: " << s_errorCode.message() << endl;
    return false;
}
