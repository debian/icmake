#define XERR
#include "modules.ih"

    // by mark.cc

void Modules::writeUsers(StrSet const &users) const
{
    ofstream out = Exception::factory<ofstream>(d_options.markFile());
    for (string const &filename: users)
        out << filename << '\n';
}
