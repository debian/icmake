#define XERR
#include "modules.ih"

    // called by addextern.cc and connect.cc

        // extModgcm: extern/Module.gcm
        // find the Mod.gcm files required by extModgcm

bool Modules::addRequiredBy(Path const &extModgcm)
{
    unique_ptr<Process> 
        procPtr{ new Process{ Process::COUT | Process::IGNORE_CERR } };

    procPtr->setBufSize(512);
    *procPtr = "/bin/strings " + extModgcm.string();
    procPtr->start();                       // find 'export:/import:' NTBSs

    string ntbs;
    while (getline(*procPtr, ntbs))         // get all the NTBSs
    {
                                            // ntbs starts with /
        if (needsGcm(ntbs))                 // a needed gcm now in ntbs
        {
            Path needed{ extModgcm.parent_path() += ntbs };

            imsg << "       adding " << needed << " required by " <<
                    extModgcm << endl;

            addExtern( needed );
        }
    }

    return true;
}
