#define XERR
#include "modules.ih"

    // called by solvedUnknown

    // 'extModgcm'  is, e.g., 'extern/Mod1.gcm'
    // scan's modvect[idx] is, e.g., 'ModName.gcm'

bool Modules::requiredExternal(Path const &extModgcm)
{
    for (size_t idx: d_unknowns)        // inspect all unknown modules
    {
        if (linked(extModgcm, idx))     // extModgcm was an unknown module
            return true;
    }
    return false;
}




