#define XERR
#include "modules.ih"

bool Modules::clean()
{
    if (not d_options.clean())
        return false;

    d_classes.reset();

    imsg << "Removing the gcm.cache/ subdir and soft-links" << endl;

    while (true)
    {
        Classes::Info info = d_classes.next();
        if (info.nr == 0)
            break;

        if (not fs::remove(info.subdir + "/gcm.cache", s_errorCode))
            wmsg << "can't remove " << info.subdir << "/gcm.cache" << endl;
    }

    if (not fs::remove_all("gcm.cache", s_errorCode))
        wmsg << "can't remove gcm.cache" << endl;

    return true;
}
