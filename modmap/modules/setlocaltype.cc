#define XERR
#include "modules.ih"

    // called by definemodule.cc

// static
bool Modules::setLocalType(ModData &data, Path const &source)
{
    if (data.type() == ModData::LOCAL)
        return false;

    data.setLocal(source);
    return true;
}
