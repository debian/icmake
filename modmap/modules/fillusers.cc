#define XERR
#include "modules.ih"

    // by mark.cc

Modules::StrSet Modules::fillUsers() const
{
    StrSet users;

    for (ModData const &data: d_modVect)        // add the module users
        users.insert(data.users().begin(), data.users().end());

    return users;
}
