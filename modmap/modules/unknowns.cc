#define XERR
#include "modules.ih"

    // called by solveunknown

bool Modules::unknowns()
{
    for (size_t idx = 0, end = d_modVect.size(); idx != end; ++idx)
    {
        if (d_modVect[idx].type() == ModData::UNKNOWN)
            d_unknowns.insert(idx);
    }

    return not d_unknowns.empty();          // true: unknowm modules exist
}
