#define XERR
#include "modules.ih"

    // called by fill.cc

    // inspect each of the (e.g. CLASSES) subdirs
void Modules::inspectSubdirs()
{
    while (true)
    {                       // next() returns idx 0 after visiting all dirs
        Classes::Info info = d_classes.next();

        if (info.nr == 0)
            break;

        inspect(info);
    }
}
