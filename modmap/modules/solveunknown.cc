#define XERR
#include "modules.ih"

    // called by fill

void Modules::solveUnknown()
{                               // UNKNOWN modules, but no -external dirs
    if (unknowns() and d_options.noExternals()) 
    {
        emsg << d_unknowns.size() << " UNKNOWN module";
        if (d_unknowns.size() > 1)
            emsg.put('s');
        emsg << endl;
        return;
    }

    if (unkownExternals())
    {
        for (size_t idx: d_unknowns)
            emsg << "Module " << d_modVect[idx].modName() << 
                                                        " not found" << endl;
    }
}
