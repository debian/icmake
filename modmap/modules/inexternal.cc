#define XERR
#include "modules.ih"

    // called by unknownexternals.cc

bool Modules::inExternal(string const &dir)
{
                                // all dir/name entries in this directory
    for (auto entry: DirIter{ dir })
    {
        if (requiredExternal( entry.path() ))
            return true;
    }

    return false;               // none here is a required module
}
