#define XERR
#include "modules.ih"

    // called by solvedUnknown

bool Modules::unkownExternals()
{
    for (string const &dir: d_options.externals()) // visit all external dirs
    {
        if (not inExternal(dir))        // no unknown mod. in d_external[idx]?
            continue;                   // then try the next directory

        if (d_unknowns.empty())
            return false;
    }
    return true;
}
