#define XERR
#include "modules.ih"

    // by ~/main.cc

void Modules::mark() const
{                                       // marking not requested
    if (d_options.markType() <= Options::COMPILE_DEPENDING)
        return;

    StrSet users{ fillUsers() };

    if (users.empty())
    {
        imsg << "--mark: no module users" << endl;
        return;
    }

    void (Modules::*marker)(StrSet const &) const = 
            d_options.markType() & Options::UPDATE_USER_TIMES ?
                &Modules::updateTimes
            :
                &Modules::writeUsers;

    (this->*marker)(users);             // handle the marking of user files
}
