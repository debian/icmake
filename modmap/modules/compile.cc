#define XERR
#include "modules.ih"

void Modules::compile()
{
    if (emsg.count() == 0 and d_options.compile())
        Compiler{ d_cwd, d_idx, d_modVect }.run();
}
