#define XERR
#include "modules.ih"

    // "import: "
    // "export: "
    //  012345678

    // called by addrequiredby

// static
bool Modules::needsGcm(string &ntbs)  // ntbs from 'strings gcmfile'
{
    if (                            // 'import:' or 'export:' and
        (ntbs.find("import:") == 0 or ntbs.find("export:") == 0) 
        and (ntbs[8] != '/')        // not an absolute gcm file specification
    )
    {
        ntbs.erase(0, ntbs.rfind(' '));     // keep the last word
        ntbs.front() = '/';                 // [0] = '/' for addrequiredby
        return true;
    }

    return false;
}
