#define XERR
#include "modules.ih"

    // by mark.cc

void Modules::updateTimes(StrSet const &users) const
{
    auto now{ chrono::file_clock::now() };

    for (string const &filename: users)
        fs::last_write_time(filename, now, s_errorCode);
}
