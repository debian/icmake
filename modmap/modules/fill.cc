#define XERR
#include "modules.ih"

    // by ~/main.cc

bool Modules::fill()
{
    inspectSubdirs();                       // inspect each of the subdirs
    inspect(Classes::Info{ 0, "." });       // inspect the project directory

    if (d_modVect.size() == 0 and d_options.noArgs())
        return false;
    
    solveUnknown();                         // unkown modules must be extern

    d_dependencies.show();                  // optionally show the deps.

    return true;
}


