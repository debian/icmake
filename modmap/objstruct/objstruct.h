#ifndef INCLUDED_OBJSTRUCT_
#define INCLUDED_OBJSTRUCT_

#include <iosfwd>
#include <filesystem>

#include "../support/support.h"

class ObjStruct: private Support
{
    using Path          = std::filesystem::path;

    bool d_compile;
    Path d_parent;
    Path d_objName;

    public:
        ObjStruct(std::string const &modName, Path const &source, 
                  std::string const &objDir, std::string const &objNr);

        void setCompile();                                              // .f

        bool compile() const;                                           // .f
        Path const &parent() const;                                     // .f
        Path const &objName() const;                                    // .f

    private:

        void setCompile(std::string const &modName, 
                        Path const &sourceFilename);

        Path setObjName(Path const &source, std::string const &objDir, 
                        std::string const &nr);

        static std::string parents(std::string const &source);
};
        
#include "objstruct.f"

#endif
