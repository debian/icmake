inline void ObjStruct::setCompile()
{
    d_compile = true;
}

inline bool ObjStruct::compile() const
{
    return d_compile;
}

inline ObjStruct::Path const &ObjStruct::parent() const
{
    return d_parent;
}

inline ObjStruct::Path const &ObjStruct::objName() const
{
    return d_objName;
}
