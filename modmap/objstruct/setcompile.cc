#define XERR
#include "objstruct.ih"

    // by objstruct1.cc

void ObjStruct::setCompile(string const &modName, Path const &sourceFilename)
{
    d_compile = 
            younger(sourceFilename, Path{ d_parent } /= d_objName)
        or  not fs::exists("gcm.cache/" + gcmName(modName))
        or  Options::instance().cmpLibrary(sourceFilename);
}
