#define XERR
#include "objstruct.ih"

    // called from setobjname.cc

// static
string ObjStruct::parents(string const &source)
{
    string ret;

    if (source.front() == '.')          // top level directory
        return ret;

    size_t pos = 0;
    
    while (true)
    {
        pos = source.find('/', pos);    // find a dir. separator
        if (pos == string::npos)
            break;
        ++pos;
        ret += "../";
    }
    return ret;
}

