#define XERR
#include "objstruct.ih"

    // called from dependencies/objfilenames.cc

ObjStruct::ObjStruct(string const &modName, Path const &source, 
                     string const &objDir, string const &nr)    // obj file nr
:
    d_parent(source.parent_path())
{
    setCompile(modName, setObjName(source, objDir, nr));
}
