#define XERR
#include "objstruct.ih"

    // called from objstruct1.cc

ObjStruct::Path ObjStruct::setObjName(Path const &source, 
                                      string const &objDir, string const &nr)
{
     if (objDir.empty())
        d_objName = nr;
    else
        (d_objName = parents(source.string()) + objDir) /= nr;

    d_objName += source.filename().replace_extension(".o");

    return source;
}

//    Path filename{ source.filename() };
//    d_objName += filename.replace_extension(".o");
