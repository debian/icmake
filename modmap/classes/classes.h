#ifndef INCLUDED_CLASSES_
#define INCLUDED_CLASSES_

#include <vector>
#include <string>

class Options;

class Classes
{
    using StrVect = std::vector<std::string>;  

    Options const &d_options;

    StrVect d_classes;      // the classes in CLASSES
    size_t d_nr;            // the sequence nr of the entries in CLASSES,
                            // d_nr 0 is used for the main directory, 

    public:
        struct Info
        {
            size_t nr;
            std::string const &subdir;
        };

        Classes();
        Info next();            // idx 0: all classes done
        size_t reset();         // reset d_idx to d_classes.size()

    private:
        void read();                            // read CLASSES
        void subdirs();                         // determine all subdirs
};

inline size_t Classes::reset()
{
    return d_nr = 0;
}
        
#endif
