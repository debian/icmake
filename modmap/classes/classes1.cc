#define XERR
#include "classes.ih"

Classes::Classes()
:
    d_options(Options::instance()),
    d_nr(0)
{
    if (d_options.ignore().empty())
        read();
    else
        subdirs();
}
