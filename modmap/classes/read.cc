#define XERR
#include "classes.ih"

void Classes::read()
{
    if (not fs::exists("CLASSES"))
        throw Exception{} << 
            "no CLASSES file. Use option --help for a brief usage summary";

                                        // open the CLASSES file
    ifstream classes{ Exception::factory<ifstream>("CLASSES") };

    string entry;                                       // get all 1st words
    while (classes >> entry and classes.ignore(1000, '\n'))   
    {
        if ("#/"s.find(entry[0]) == string::npos)       // skip comment lines
            d_classes.push_back(move(entry));           // add the class name
    }
}

