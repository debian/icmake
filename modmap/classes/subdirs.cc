#define XERR
#include "classes.ih"

void Classes::subdirs()
{
    StrVect const &ignore = d_options.ignore();

    for (char const *dirname: Glob{ Glob::DIRECTORY, "*", Glob::NOMATCH })
    {
        if (find(ignore.begin(), ignore.end(), dirname) == ignore.end())
            d_classes.push_back(dirname);
    }
}
