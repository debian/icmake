#define XERR
#include "classes.ih"

Classes::Info Classes::next()
{
    if (d_nr == d_classes.size())
        return Info{ 0, "" };           // nr 0: all entries processed

    Info ret{ d_nr + 1, d_classes[d_nr] };
    ++d_nr;
    return ret;
}
