#ifndef INCLUDED_MODDATA_
#define INCLUDED_MODDATA_

#include <unordered_set>
#include <string>
#include <filesystem>

#include "../objstruct/objstruct.h"

struct ModData
{
    enum Type                   // module type (see also data.cc)
    {
        UNKNOWN,
        LOCAL,
        EXTERN
    };

    private:
        using Path          = std::filesystem::path;
        using Set           = std::unordered_set<size_t>;
        using StrSet        = std::unordered_set<std::string>;

        Type d_type;
        Path d_source;          // source path of this module relative to cwd 
        std::string d_modName;  // name of this module

        Set d_imports;          // modules imported by this module

        StrSet d_users;         // source files importing this module

        size_t d_nr;            // prefix before .o filenames

        ObjStruct *d_objStruct;

        static char const *s_type[];    // module type NTBSs

    public:
        ModData(std::string const &modName,     // by default UNKNOWN
                Type type = UNKNOWN,
                Path const &source = Path{});

        ~ModData();                                                     // .f

        bool compile() const;                                           // .f

        Set const &imports() const;                                     // .f

        void imports(size_t idx);                                       // .f

        StrSet const &users() const;                                    // .f
        void user(Path const &source);                                  // .f

        std::string const &modName() const;                             // .f

        void setExtern();                                               // .f

        void setLocal(Path const &source);                              // 1
        void setLocal(size_t nr, Path const &source);                   // 2

        void setCompile();                                              // .f
        void setObjStruct(std::string const &objDir);                   // .f

        Path const &objName() const;                                    // .f
        Path const &sourceDir() const;                                  // .f

        std::string nr() const;                                         // .f
        Path const &source() const;                                     // .f

        std::string sourceFilename() const;                             // .f
//        std::string sourceStr() const;                                  // .f

        Type type() const;                                              // .f
        char const *typeTxt() const;                                    // .f
};

#include "moddata.f"
        
#endif



