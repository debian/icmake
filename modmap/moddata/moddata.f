inline ModData::~ModData()
{
    delete d_objStruct;
}

inline void ModData::setExtern()
{
    d_type = EXTERN;    
}

inline bool ModData::compile() const
{
    return d_objStruct != 0 and d_objStruct->compile();
}

inline void ModData::setCompile()
{
    d_objStruct->setCompile();
}

    // dependencies/objfilenames.cc
inline void ModData::setObjStruct(std::string const &objDir)
{
    d_objStruct = new ObjStruct{ d_modName, d_source, objDir, nr() };
}

inline ModData::Set const &ModData::imports() const
{
    return d_imports;
}

inline void ModData::imports(size_t idx)
{
    d_imports.insert(idx);
}

inline ModData::Path const &ModData::sourceDir() const
{
    return d_objStruct->parent();
}

inline ModData::StrSet const &ModData::users() const
{
    return d_users;
}

inline void ModData::user(Path const &source)
{
    d_users.insert(source.string());
}

inline ModData::Path const &ModData::source() const
{
    return d_source;
}
        
inline ModData::Path const &ModData::objName() const
{
    return d_objStruct->objName();
}
        
inline std::string ModData::sourceFilename() const
{
    return d_source.filename().string();
}
        
inline std::string const &ModData::modName() const
{
    return d_modName;
}

inline char const *ModData::typeTxt() const
{
    return s_type[d_type];
}
        
inline ModData::Type ModData::type() const
{
    return d_type;
}

inline std::string ModData::nr() const
{
    return std::to_string(d_nr);
}



