#define XERR
#include "moddata.ih"

ModData::ModData(string const &modName, Type type, Path const &source)
:
    d_type(type),
    d_source(source),
    d_modName(modName),
    d_objStruct(0)
{}

