#define XERR
#include "moddata.ih"

char const *ModData::s_type[]       // names of Type enum values
    {
        "UNKNOWN",
        "LOCAL", 
        "EXTERN",
    };
