#define XERR
#include "compiler.ih"

    // run.cc (via sethandler.cc)

void Compiler::toFile()
{
    imsg << "Writing interface units to compile to " << 
                                                d_options.filename() << endl;

    for (size_t idx: d_idx)                 // compile in the order specified
    {                                       // by d_idx's elements
        ModData const &data = d_modVect[idx];   

         if (data.compile())                
            *d_filePtr << data.source().string() << '\n';
    }
}

