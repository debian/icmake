#define XERR
#include "compiler.ih"

    // by compilemods.cc / compiler1.cc

// static 
void Compiler::noop()
{}

// static
void Compiler::informOnce()
{
    cout << "Compiling:\n";
    s_inform = noop;
}
