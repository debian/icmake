#define XERR
#include "compiler.ih"

void Compiler::run()
{
    if (d_options.compile())
        (this->*d_handler)();           // compileMods or toFile
}
