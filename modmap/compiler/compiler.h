#ifndef INCLUDED_COMPILER_
#define INCLUDED_COMPILER_

#include <vector>
#include <fstream>
#include <string>

#include "../support/support.h"
#include "../moddata/moddata.h"

namespace FBB
{
    class Process;
}

class Options;

class Compiler: private Support
{
    using Path          = std::filesystem::path;
    using ModVect       = std::vector<ModData>;
    using IdxVect       = std::vector<size_t>;

    Options const &d_options;
    std::string const &d_cwd;
    IdxVect const &d_idx;
    ModVect const &d_modVect;

    std::string d_std;          // used by the compiler

    std::ofstream d_file;       // file used when option --file is spcecified
    std::ostream *d_filePtr;    // ptr to the actually used output file
    void (Compiler::*d_handler)();

    static std::string s_compileCmd;
    static void (*s_inform)();;

    public:
        Compiler(std::string const &cwd,    // also requests ICMAKE_CPPSTD
                 IdxVect const &idxVect,  ModVect const &modVect);

        void run();

    private:
        void compileMods();
        void compile(FBB::Process &proc, ModData const &data);
                    // Path const &dir,
                    // std::string const &source, std::string const &objFile);

        void setHandler();
        void toFile();

        static void informOnce();
        static void noop();             // informonce.cc

};    
        
#endif
