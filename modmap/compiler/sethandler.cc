#define XERR
#include "compiler.ih"

void Compiler::setHandler()
{
    if (string const &filename = d_options.filename(); filename.empty())
        d_handler = &Compiler::compileMods;
    else
    {
        if (d_options.stdCout())                        // use cout
            d_filePtr = &cout;
        else
        {
            d_file = Exception::factory<ofstream>(filename);
            d_filePtr = &d_file;
        }
        d_handler = &Compiler::toFile;
    }
}
