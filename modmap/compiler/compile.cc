#define XERR
#include "compiler.ih"

// cstdlib:
//     std::cout << getenv("ICMAKE_CPPSTD") << '\n';
// returns, e.g., --std=c++26
//
// d_std is set in compiler1.cc

    // called from compilemods.cc

            // Path const &dir, string const &source, string const &objFile)

void Compiler::compile(Process &proc, ModData const &data)
{
    string cmd = "/bin/g++ " + d_std + d_options.colors() + s_compileCmd + 
                        data.objName().string() +  ' ' + 
                        data.sourceFilename();

                        // objFile + ' ' + source;

    if (d_options.show() & Options::COMPILER_CALLS)
        cout << "   in " << data.sourceDir().string() << ": " << cmd <<'\n';

    proc = cmd;
    proc.start();

    if (proc.waitForChild() != 0)
    {
        emsg << cmd << " failed\n";
        throw 1;
    }
}
