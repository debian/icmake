#define XERR
#include "compiler.ih"

    // run.cc (via sethandler.cc)

void Compiler::compileMods()
{
    unique_ptr<Process> 
        procPtr{ new Process{ Process::NONE, Process::NO_PATH } };

    procPtr->setBufSize(512);

    for (size_t idx: d_idx)                 // compile in the order specified
    {                                       // by d_idx's elements
        ModData const &data = d_modVect[idx];   

                                            // not-compiled/EXTERN module
        if (data.type() == ModData::EXTERN or not data.compile())
            continue;

        setDir(data.sourceDir());            // cd to the source's dir
        
        (*s_inform)();

        compile(*procPtr, data);    
    
        setDir(d_cwd);                       // back to the cwd
    }
}



