#include "compiler.ih"

Compiler::Compiler(string const &cwd, IdxVect const &idxVect, 
                   ModVect const &modVect)
:
    d_options(Options::instance()),
    d_cwd(cwd),
    d_idx(idxVect),
    d_modVect(modVect)
{
    if (char const *std = getenv("ICMAKE_CPPSTD"); std != 0)
        d_std = std;

    s_inform =  (d_options.show() & Options::COMPILER_CALLS) != 0 
                or imsg.isActive() ? informOnce : noop;

    setHandler();
}
