#ifndef INCLUDED_DEPENDENCIES_
#define INCLUDED_DEPENDENCIES_

#include <iosfwd>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <filesystem>

#include "../support/support.h"
#include "../moddata/moddata.h"

class Options;
class Scan;

class Dependencies: private Support
{
    using Path          = std::filesystem::path;
    using Set           = std::unordered_set<size_t>;

    using ModVect       = std::vector<ModData>;
//    using VectIter      = Vect::iterator;

    using ModMap        = std::unordered_map<std::string, size_t>;

    using SetVect       = std::vector<Set>;

    using IdxVect       = std::vector<size_t>;
    using IdxVectIter   = IdxVect::iterator;
 
    Options const &d_options;

    IdxVect &d_idx;

    ModVect &d_modVect;
    ModMap const &d_moduleNameIdx; // from module name to d_modVect idx.

    SetVect d_circular;

    SetVect d_reverse;      // indices of modified interface units and 
                            // the indices of interface units that depend
                            // on them (used with COMPILE_DEPENDING

    public:
        Dependencies(IdxVect &idxVect, ModVect &modVect, 
                     ModMap const &modMap);

        bool circular();
        bool objFilenames();
        void show() const;

    private:
        void fill(size_t idx);
        void impliedCompilations();
        void impliedBy(size_t unit);
        IdxVectIter rmIndices(IdxVectIter from, IdxVectIter const &next);
        void setCircular();
        void setCompile(size_t unit);
        void setObjStructs();
        
        static void showMod(ModData const &data);
};
        
#endif




