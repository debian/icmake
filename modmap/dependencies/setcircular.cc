#define XERR
#include "dependencies.ih"

    // called by circular.cc

void Dependencies::setCircular()
{
    for (ModData &data: d_modVect)
        d_circular.push_back(data.imports());      // cp d_imports to ret

    d_idx.resize(d_modVect.size());
    iota(d_idx.begin(), d_idx.end(), 0);           // fill the indices
}
