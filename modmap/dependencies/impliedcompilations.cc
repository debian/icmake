#define XERR
#include "dependencies.ih"

    // by objfilenames.cc

void Dependencies::impliedCompilations()
{
    if (not d_options.compileDepending())
        return;

    d_reverse.resize(d_modVect.size());     // indices of depending units

                                            // visit all modules
    for (size_t idx = 0, end = d_modVect.size(); idx != end; ++idx)
        fill(idx);                 // if [idx] imports local units
                                            // then idx depends on those

    for (size_t idx = 0, end = d_modVect.size(); idx != end; ++idx)
        setCompile(idx);                    // if unit[idx] is compiled then
                                            // set compile of units depending
                                            // on unit[idx]
}
