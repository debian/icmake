#define XERR
#include "dependencies.ih"

    // by impliedcompilations.cc

void Dependencies::setCompile(size_t unit)
{
    if (d_modVect[unit].compile())          // this unit is compiled:
        impliedBy(unit);                      // compile units depending on it.
}
