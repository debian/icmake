#define XERR
#include "dependencies.ih"

    // called from modules/modules.f

    // d_circular contains the d_imports from the d_modVect modules
    // d_idx[idx] initially contains indices 0..N, but is partitioned 
    // so that 0-importing indices are from 0..first

bool Dependencies::circular()
{
    setCircular();                              // cp modVect's imports, 
                                                // set d_idx
    IdxVectIter from = d_idx.begin();

    while (true)
    {
        IdxVectIter next =          // the 0-dependent module indices on top
                    partition(from, d_idx.end(), 
                        [&](size_t idx)
                        {
                            return d_circular[d_idx[idx]].size() == 0;
                        }
                    );

        if (next == d_idx.end())            // all circular checks were passed
            return false;                   // no circular deps

        if (from == next)                   // circ.deps: deps still exist
        {
            emsg << "can't compile module " << d_modVect[*from].modName() << 
                    ": circular or undefined module(s)\n" << endl;
            return true;
        }

        from = rmIndices(from, next);   // remove indices from to next from
                                        // d_circular
    }
}
