#define XERR
#include "dependencies.ih"

    // by impliedcompilations.cc

void Dependencies::fill(size_t idx)
{
                                        // visit the unit's imports
    for (size_t importIdx: d_modVect[idx].imports())  
    {
                                        // if it imports a local unit then
                                        // unit[idx] depends on it
        if (d_modVect[importIdx].type() == ModData::LOCAL)
            d_reverse[importIdx].insert(idx);
    }
}
