#define XERR
#include "dependencies.ih"

// static
void Dependencies::showMod(ModData const &data)
{
    cout << data.typeTxt() << ' ' <<
        (data.modName().find(':') == string::npos ? "module" : "partition") << 
        ' ' << data.modName() << '\n';
}
