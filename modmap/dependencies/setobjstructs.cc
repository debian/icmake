#define XERR
#include "dependencies.ih"

    // by objfilenames.cc

void Dependencies::setObjStructs()
{
    string const &objDir = d_options.objDir();

    for (ModData &data: d_modVect)      // visit all modules
    {
        if (data.type() == ModData::EXTERN)
            continue;

        data.setObjStruct(objDir);
    }

}
