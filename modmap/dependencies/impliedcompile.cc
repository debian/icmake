#define XERR
#include "dependencies.ih"

    // by setcompile.cc

void Dependencies::impliedBy(size_t unit)
{
    for (size_t idx: d_reverse[unit])
    {
        if (d_modVect[idx].compile())       // idx will already be compiled
            continue;

        d_modVect[idx].setCompile();        // implied compilation
        impliedBy(idx);                     // and the ones depending on it
    }
}
