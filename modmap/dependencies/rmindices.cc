#define XERR
#include "dependencies.ih"

    // called by circular.cc

    // rm indices in d_circular[*from] until from == next from 
    // d_circular[*(next..d_idx.end()]

Dependencies::IdxVectIter Dependencies::rmIndices(IdxVectIter from, 
                                                  IdxVectIter const &next)
{
    for (IdxVectIter begin = next, end = d_idx.end(); begin != end; ++begin)
    {
        Set &imports = d_circular[*begin];

        for (IdxVectIter rmIter = from; rmIter != next; ++rmIter)
            imports.erase(*rmIter);
    }

    return next;
}
