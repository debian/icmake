#define XERR
#include "dependencies.ih"

void Dependencies::show() const
{
    if ((d_options.show() & Options::DEPENDENCIES) == 0)
        return;

    cout << "Dependencies:\n";

    for (ModData const &data: d_modVect)        // all interface units in this 
    {                                           // project
        cout << "   ";
        showMod(data);

        for (size_t idx: data.imports())
        {
            cout << "       imports ";
            showMod(d_modVect[idx]);
        }
    }
}
