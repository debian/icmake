#define XERR
#include "dependencies.ih"

    // called from modules/modules.f

bool Dependencies::objFilenames()
{
    if (not d_options.needObjStructs()) // no further actions requested
        return false;

    setObjStructs();

    impliedCompilations();              // set compile of depending units

    return true;
}
