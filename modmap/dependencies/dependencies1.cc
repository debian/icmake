#define XERR
#include "dependencies.ih"

    // called by modules1.cc

Dependencies::Dependencies(IdxVect &idxVect,
                           ModVect &modVect, ModMap const &modMap)
:
    d_options(Options::instance()),
    d_idx(idxVect),
    d_modVect(modVect),
    d_moduleNameIdx(modMap)
{}
