#define XERR
#include "support.ih"

bool Support::isDirSymlink(DirEntry const &entry) const
{
    return 
        fs::is_symlink(fs::symlink_status(entry)) 
        and 
        fs::is_directory(fs::status(entry));
}
