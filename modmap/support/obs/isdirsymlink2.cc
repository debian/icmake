#define XERR
#include "support.ih"

bool Support::isDirSymlink(Path const &entry) const
{
    return isDirSymlink(DirEntry{ entry });
}
