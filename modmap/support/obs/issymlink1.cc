#define XERR
#include "support.ih"

    // entry may be empty, in which case 'false' is returned

bool Support::isSymlink(DirEntry const &entry) const
{
    return fs::is_symlink(fs::symlink_status(entry.path()));
}
