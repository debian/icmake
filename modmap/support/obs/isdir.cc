#define XERR
#include "support.ih"

bool Support::isDir(DirEntry const &entry) const
{
    return fs::is_directory(fs::status(entry));
}
