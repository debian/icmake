#define XERR
#include "support.ih"

// static
bool Support::younger(Path const &srcFile, Path const &objFile)
{
    Stat obj{ objFile.string() };

    return not obj?             // objFile doesn't exist
                true
            :
                Stat{ srcFile.string() }.lastModification() >=
                                                    obj.lastModification();
}
