#ifndef INCLUDED_SUPPORT_H_
#define INCLUDED_SUPPORT_H_

#include <system_error>
#include <filesystem>

class Support
{
    using Path = std::filesystem::path;

    protected:
        static std::error_code s_errorCode;

        static void createSymlink(Path const &dest, Path const &link);

        static bool younger(Path const &srcFile,        // also if objFile 
                     Path const &objFile);              // doesn't exist

        static std::string col2hyphen(std::string const &name);
        static std::string gcmName(std::string const &name);

        static void setDir(Path const &dirname);
};

    // static   return 'name' as a gcm filename
inline std::string Support::gcmName(std::string const &name)
{
    return col2hyphen(name) + ".gcm";
}


#endif

