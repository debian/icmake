#define XERR
#include "support.ih"

void Support::setDir(Path const &dirname)
{
    fs::current_path(dirname, s_errorCode);
    if (s_errorCode)
        throw Exception{} << "cannot chdir to " << dirname;
}
