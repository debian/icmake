#define XERR
#include "support.ih"

// static
void Support::createSymlink(Path const &dest, Path const &link) 
{
    string msg { "soft-link from " + link.string() + " -> " + dest.string() };

    imsg << "Defining a " << msg << endl;
    fs::create_symlink(dest, link, s_errorCode);
    if (s_errorCode and s_errorCode != errc::file_exists)
        emsg << "could not create the " << msg << endl;
}
