#define XERR
#include "support.ih"

// static
string Support::col2hyphen(string const &name)
{
    string ret{ name };

    for (char &ch: ret)
    {
        if (ch == ':')
            ch = '-';
    }

    return ret;
}
