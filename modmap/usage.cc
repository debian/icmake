#include "main.ih"

namespace {
char const info[] = R"_( [options] [arg]
Where:
    [options] - optional arguments (short options between parentheses, quotes
                in the descriptions must be omitted when used as option
                arguments): 
       --colors (-c)        - the compiler uses diagnostic colors 
                              (default: no colors).
       --dependencies (-d)  - no compilation, merely show the dependencies
       --extension (-x) ext - ext: source file extension. By default .cc
       --extern (-e) arg    - the currrent project's .gcm files may depend on
                              .gcm files defined by other (external)
                              projects. Those external .gcm files are located
                              in the directory 'arg' or in the directory
                              location(s) argified in separate
                              lines of the file `arg' (ignoring its empty
                              lines and lines starting with //.
                              Those external .gcm files (and the .gcm files
                              they depend on) are soft-linked to the project's
                              gcm.cache directory.
       --help (-h)          - provide this help and end the program
       --ignore (-i) list   - all subdirs except for those listed in 'list'
                              are visited, CLASSES is not used. 
                              Use --ignore '' to visit all subdirs.
       --library (-l) lib   - the path (relative to the project's top-level
                              directory) to a file containing the project's .o
                              files.
       --mark (-m) arg      - arg can be +, ++, or a filename. When --mark is
                              specified interface units depending on other
                              modified interface units are also compiled.  
                              If arg is '++' then the last write times of all
                              files implementing or using those interface
                              units are set to the current time.
                              If arg isn't + or ++ then it specifies a
                              filename to receive the names of the files
                              otherwise processed by '++'.
       --quiet (-q) what    - by default compiler calls and dependencies are 
                              written to the std. output stream. 
                              Specify 'what' as:
                                c - to suppress showing the compiler commands
                                d - to suppress showing the dependencies.
                                  Anything else: both 'c' and 'd'
                              --verbose suppresses --quiet
       --subdir (-s)        - the compiled module/partition files (.o files)
                              are located in in their source file directories.
                              By default in the project's tmp/o sub-directory.
                              Each .o file is prefixed by a unique number
                              (e.g., 1module.o, 2module.o, ...).
       --version (-v)       - show version information and end the program.
       --verbose (-V)       - show details of performed steps.

    arg - the action to perform:
        no argument - perform the default cconfigured module-mapping according
            to the specified options (no options: using CLASSES and performing
            compilations). 
            
        clean - remove the gcm.cache directories in the current working
                directory and in the project's subdirectories

        other argument - the module/partition source files are not compiled,
                but their names (beyond the current working directory) are
                written in the order they should be compiled to the stdandard
                output stream if arg is - and otherwise to the file whose name
                is specified by arg.
        
    Files are compiled using the C++ standard compiler option specified by the
    ICMAKE_CPPSTD environment variable. If ICMAKE_CPPSTD isn't defined the
    compiler's default C++ standard is used.
)_";

//       --file (-f) name     - the module/partition source files are not
//                              compiled, but their names (beyond the current
//                              working directory) are written to 'name'. Use
//                              name '-' to write the namesn to the stdandard
//                              output stream.

}

void usage(std::string const &progname)
{
    Tools::usageTop(progname) << info;
}
