#include "main.ih"

namespace
{
    Arg::LongOption longOpts[] =
    {
        Arg::LongOption{"clean",        'C'},
        Arg::LongOption{"colors",       'c'},
        Arg::LongOption{"dependencies", 'd'},
        Arg::LongOption{"extern",       'e'},
        Arg::LongOption{"extension",    'x'},
        Arg::LongOption{"help",         'h'},
        Arg::LongOption{"ignore",       'i'},
        Arg::LongOption{"library",      'l'},
        Arg::LongOption{"mark",         'm'},
        Arg::LongOption{"quiet",        'q'},
        Arg::LongOption{"subdir",       's'},
        Arg::LongOption{"version",      'v'},
        Arg::LongOption{"verbose",      'V'},
    };
    auto longEnd = longOpts + size(longOpts);
}

int main(int argc, char **argv)
try
{
    Options const &options = 
        Options::initialize("cCde:hi:l:m:q:svVx:", longOpts, longEnd, 
                            argc, argv, VERSION, usage);

    Modules modules;        // CLASSES or --noclasses must be present
                            // defines gcm.cache, tmp/o and gcm.cache/usr
    if (modules.clean())
        return 0;

    if (not modules.fill())         // determine the locations of the modules
    {
        usage(options.basename());
        cout << "\n"
                "    [Warning]\n"
                "       No options or arguments were specified and\n"
                "       no module interface units were found\n" << endl;

        return 1;
    }

    if (modules.circular()) // may also determine the interface units of 
        return 1;           // units depending on modified ones

    if (not modules.objFilenames()) // determine obj. filenames and 
        return 0;                   // compilation request

    modules.mark();         // maybe mark files using modules
    modules.compile();      // compile files or write their names to a file
}
catch (...)
{
    return handleException();
}

