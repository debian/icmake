#define XERR
#include "options.ih"

void Options::setLibrarytime()
{
    string libName;

    d_libraryTime = 0;          // 0: cmpLibrary returns false

    if (not d_arg.option(&libName, 'l'))
        return;

    if (Stat libStat{ libName }; libStat)
    {
        d_libraryTime = libStat.lastModification().utcSeconds();
        return;
    }

    wmsg << "--library " << libName << " does not (yet) exist" << endl;
}
