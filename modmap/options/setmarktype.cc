#define XERR
#include "options.ih"

void Options::setMarkType()
{
    string arg;
    if (not d_arg.option(&arg, 'm'))
    {
        d_markType = NO_MARK;
        return;
    }

    d_markType = COMPILE_DEPENDING;

    if (arg == "++")
        d_markType |= UPDATE_USER_TIMES;
    else if (arg.front() != '+')
    {
        d_markFile = arg;
        d_markType |= WRITE_USER_NAMES;
    }
}
