#define XERR
#include "options.ih"

//      --------------------------------------
//                        show
//             ------------------------------
//      -q     dependencies    compiler calls
//      --------------------------------------
//                 1               1            -q not used (= --verbose)
//       c         1               0
//       d         0               1
//       *         0               0            *: any other argument
//      --------------------------------------


Options::Options(char const *optString, 
                Arg::LongOption const *const begin,  
                Arg::LongOption const *const end,  
                int argc, char **argv, char const *version, 
                void (*usage)(std::string  const  &))
:
    d_arg(Arg::initialize(optString, begin, end, argc, argv)),
    d_stdCout(false)
{
    d_arg.versionHelp(usage, version, 0);       // on request: -h or -V

    inspectVerbose();

    if (d_arg.nArgs() == 0)
        d_clean = false;
    else
    {
        if (d_arg[0] == "clean"s)
        {
            d_clean = true;
            return;                         // action 'clean' requested
        }

        if (d_arg[0] != "-"s)
            d_filename = d_arg[0];
        else
        {
            d_filename = "std::cout";
            d_stdCout = true;
        }
    }

    setExtension();

    if (not d_arg.option('c'))
        d_colors = " -fdiagnostics-color=never ";

    setShow();

    if (not d_arg.option('s'))
        d_objDir = "tmp/o/";        // prefixing series of ../ in compilemods

    fillIgnore();
    setExternals();
    setLibrarytime();
    setMarkType();
}
