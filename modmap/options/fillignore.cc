#define XERR
#include "options.ih"

void Options::fillIgnore()
{
    string list;
    if (not d_arg.option(&list, 'i'))
        return;

    String::split(&d_ignore, list);
}
