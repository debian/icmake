#define XERR
#include "options.ih"

void Options::inspectVerbose()
{
    d_dependencies = d_arg.option('d');   // show the dependencies
    d_verbose = d_arg.option('V');

    if (not d_verbose)
    {
        if (d_dependencies and d_arg.option('f'))
            wmsg << "Option --dependencies suppresses --file" << endl;
        imsg.off();
    }
}
