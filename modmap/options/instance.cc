#include "options.ih"

// static
Options const &Options::instance()
{
    if (not s_options)
        throw Exception{} << "Options not yet initialized";
    
    return *s_options;
}
