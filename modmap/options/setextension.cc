#define XERR
#include "options.ih"

void Options::setExtension()
{
    if (not d_arg.option(&d_extension, 'x'))
        d_extension = ".cc";
    else if (d_extension.front() != '.')            // extensions start at .
        d_extension.insert(0, 1, '.');
}
