inline bool Options::clean() const
{
    return d_clean;
}

inline bool Options::compile() const
{
    return not d_dependencies;
}

inline bool Options::compileDepending() const
{
    return (d_markType & COMPILE_DEPENDING) != 0;
}

inline bool Options::dependencies() const
{
    return d_dependencies;
}

inline bool Options::noArgs() const
{
    return d_arg.nArgs() == 0 and d_arg.nOptions() == 0;
}

inline bool Options::stdCout() const
{
    return d_stdCout;
}

inline bool Options::needObjStructs() const
{
    return not d_dependencies or d_markType != 0;
}

inline bool Options::verbose() const
{
    return d_verbose;
}

inline bool Options::setUsers() const
{
    return d_markType > COMPILE_DEPENDING;
}

inline unsigned Options::markType() const
{
    return d_markType;
}

inline unsigned Options::show() const
{
    return d_show;
}

inline std::string const &Options::basename() const
{
    return d_arg.basename();
}

inline std::string const &Options::extension() const
{
    return d_extension;
}

inline std::string const &Options::colors() const
{
    return d_colors;
}

inline std::string const &Options::markFile() const
{
    return d_markFile;
}

inline Options::StrVect const &Options::externals() const
{
    return d_externals;
}

inline bool Options::noExternals() const
{
    return d_externals.empty();
}

inline std::string const &Options::filename() const
{
    return d_filename;
}

inline Options::StrVect const &Options::ignore() const
{
    return d_ignore;
}

inline std::string const &Options::objDir() const
{
    return d_objDir;
}


