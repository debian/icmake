#include "options.ih"

// static
Options const &Options::initialize(
                            char const *optString, 
                            Arg::LongOption const *const begin,  
                            Arg::LongOption const *const end,  
                            int argc, char **argv, char const *version, 
                            void (*usage)(string  const  &)
                        )
{
    if (s_options)
        throw Exception{} << "Options already initialized";

    s_options = unique_ptr<Options>{ 
                    new Options{ optString, begin, end, argc, argv, 
                                 version, usage }
                };

    return *s_options;
}
