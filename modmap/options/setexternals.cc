#define XERR
#include "options.ih"

void Options::setExternals()
{
    string name;
    if (not d_arg.option(&name, 'e'))               // no -e option
        return;

                                                    // a direct symlink
    if (not fs::is_regular_file(fs::status(name, s_errorCode)))
    {
        if (name.back() != '/')                     // dirs end in '/'
            name += '/';

        d_externals.push_back(name);                // then a single location
        
        return;
    }

    ifstream in{ Exception::factory<ifstream>(name) };

    string dir;
    while (getline(in, dir))                        // read the lines
    {                                               
                                                    // cut-off at comment
        if (size_t pos = dir.find("//"); pos != string::npos)  
            dir.resize(pos);

        dir = String::trim(dir);
        if (dir.empty())                            // ignore empty dirs
            continue;

        if (dir.back() != '/')                      // dirs end in '/'
            dir += '/';

        if (not fs::exists(dir))                   // dir: doesn't exist
        {
            wmsg << "entry " << dir << " in -external " << name <<
                    " does not exist" << endl;
            continue;
        }

        d_externals.push_back( move(dir) );
    }
}
