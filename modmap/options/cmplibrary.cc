#define XERR
#include "options.ih"

bool Options::cmpLibrary(Path const &source) const
{
    return d_libraryTime > 0 
            and 
            d_libraryTime < 
                static_cast<size_t>(
                    Stat{ source.string() }.lastModification().utcSeconds()
                );
}

