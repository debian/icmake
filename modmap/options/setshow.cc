#define XERR
#include "options.ih"

void Options::setShow()
{
    string arg;

    d_show =    d_verbose or not d_arg.option(&arg, 'q') ? ALL :
                arg == "c" ? DEPENDENCIES   :   // compiler calls not shown
                arg == "d" ? COMPILER_CALLS :   // dependencies not shown
                             QUIET;             // show nothing
}
