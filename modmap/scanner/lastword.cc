#include "scanner.ih"

    // e.g. ... import ident;
    //                 ^     ^
    //                 from  length()

int Scanner::lastWord(Tokens token)
{
    d_exports = matched().find("export ") != string::npos;

    size_t from = matched().find_last_of(" \t") + 1;    // 1st IDENT pos
    size_t count = matched().length() - from - 1;       // length of last word

    setMatched(matched().substr(from, count));

    return token;
}
