#define XERR
#include "scan.ih"

    // called by modules/inspect.cc

    // sourcePath the path to reach filename fm the project's top-level dir.

void Scan::process(std::string const &subdir, string const &filename)
{
    (d_sourcePath = subdir) /= filename;

    d_exportNeeded = false;         // set to true by defineModule
    d_nameIdx = ~0UL;               // reset the idx: no available module name
    d_currentIdx = ~0UL;            // reset the idx: no module defined yet
                                    // by this source

    imsg << "   scanning " << filename << endl;
    d_scanner.switchStreams(filename, "-");

    while (true)
    {
        switch (d_scanner.lex())
        {
            case 0:
            return;

            case Scanner::MODDEF:               // export module Name;
               defineModule();
            continue;

            case Scanner::PARTDEF:              // export module Name:Part;
                definePartition();
            continue;

            case Scanner::MODDECL:              // [export] import Other; 
                importModule();
            continue;

            case Scanner::PARTDECL:             // [export] import :Part;
                importPartition();
            continue;

            case Scanner::MODSRC:               // module Name;
                modSource();
            continue;      
        }
    }
}
