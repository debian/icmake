#define XERR
#include "scan.ih"

    // the name of a module/partition is in 'name':
    // if not in d_modVect, add it as UNKNOWN

size_t Scan::declare(string const &name)
{
    if (
        auto inserted = d_modNameIdx.insert({ name, d_modVect.size() });
        not inserted.second         // not .second? then existed
    )
        return idxOf(inserted);

    d_modVect.push_back(ModData{ name });
    return d_modVect.size() - 1;
}

