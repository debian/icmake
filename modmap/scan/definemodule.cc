#define XERR
#include "scan.ih"

    // the name of a defined module (export module name;) is in 
    // d_scanner.matched(). Its details are stored in Module's d_modVect and
    // d_moduleNameIdx 

     // MODDEF: export module Name;

void Scan::defineModule()
{
    string const &matched = d_scanner.matched();

    imsg << "       defines module " << matched << endl;

    d_currentIdx = define(matched);
    d_nameIdx = d_currentIdx;

    d_exportNeeded = true;
}
