#define XERR
#include "scan.ih"

    // define a module partition: the module is added to d_modVect,
    //      the parition is defined as a LOCAL d_modVect element
    //      the module imports the partition, d_currentIdx is set to
    //      the index of the partition's entry in d_modVect

    // PARTDEF:              // export module Name:Part;

void Scan::definePartition()
{
    string const &matched = d_scanner.matched();

    imsg << "       defines partition " << matched << endl;

                                    // declare the module, get its index
    d_nameIdx = declare(matched.substr(0, matched.find(':')));

                                    // define the partition Name:Part
    d_currentIdx = define(matched); // and set d_current
}

// it's defined but not yet used
//  d_modVect[idx].imports(d_currentIdx);






