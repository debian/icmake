#define XERR
#include "scan.ih"

    // MODSRC   module Name;

    // called by process.cc

void Scan::modSource()   
{
    string const &matched = d_scanner.matched();

    imsg << "       declares module " << matched << endl;

    d_nameIdx = declare(matched);

    d_modVect[d_nameIdx].user(d_sourcePath);    // sourcePath uses this module
}

//    xerr("      ***USER***: " << matched << " is used by " << d_sourcePath);
