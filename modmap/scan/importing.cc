#define XERR
#include "scan.ih"

    // called by importpartition.cc and importmodule.cc

void Scan::importing(string const &name)
{
    size_t idx = declare(name);
    if (d_currentIdx != ~0UL)                   
        d_modVect[d_currentIdx].imports(idx);   // imported by a module/part.
    else                                        // or
        d_modVect[idx].user(d_sourcePath);      // the module/partition is
                                                // used by sourcePath
}
