#define XERR
#include "scan.ih"

size_t Scan::define(string const &name)
{
    size_t idx = declare(name);

    if (d_modVect[idx].type() == ModData::UNKNOWN)
        d_modVect[idx].setLocal(d_nr, d_sourcePath);
    else
        emsg << d_sourcePath << ": module " << name << 
                " already defined in " << d_modVect[idx].source() << endl;
    
    return idx;    
}
