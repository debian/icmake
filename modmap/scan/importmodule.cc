#define XERR
#include "scan.ih"

    // the name of a module ([export] import name;) is in 
    // d_scanner.matched(): declare it in d_modVect 
    // if d_currentIdx != ~0UL then modVect[d_currentIdx] imports the module

    // MODDECL: [export] import Name; 

void Scan::importModule()
{
    string const &name = d_scanner.matched();

    imsg << "       imports module " << name << endl;

    importing(name);                // either import a module or the source
                                    // file uses the module -> users
}
