#define XERR
#include "scan.ih"

    // the name of a partition (export import :Part;) is in 
    // d_scanner.matched(): declare Module:Part in d_modVect, add it to 
    // d_currentIdx's imports

    // PARTDECL export import :Part;

void Scan::importPartition()
{
    string matched = d_scanner.matched();

    if (matched.front() != ':' and matched.find(':') != string::npos)
    {
        emsg << d_sourcePath << 
                " specifies `" << matched <<
                "': use `:{NAME}' to declare a partition" << endl;
        return;
    }


    string name = d_modVect[d_nameIdx].modName();

//xerr("partition's module name: " << name);

    name = name.substr(0, name.find(':'));          // use the module name
    name += matched;            // construct ModuleName:Partition

    imsg << "       imports partition " << name << endl;

    importing(name);

    if (d_exportNeeded and not d_scanner.exports())
        emsg << d_sourcePath <<": `import " << d_scanner.matched() <<  
                "' must start with 'export'" << endl;
}

//    if (
////    if (d_user)
////        d_modVect[idx].user(d_sourcePath);    // sourcePath uses this module
////imsg << "       " << d_sourcePath << " uses " << name << endl;
//    
//    d_modVect[d_currentIdx].imports(idx);

