#define XERR
#include "scan.ih"

    // called from modules/modules1.cc

Scan::Scan(ModVect &modVect, ModMap &modNameIdx)
:
    d_options(Options::instance()),
    d_modVect(modVect),
    d_modNameIdx(modNameIdx),
    d_scanner(cin, cout, false)     // do not keep the construction cwd
{}
