#include "main.ih"

int handleException()
try
{
    rethrow_exception(current_exception());
} 
catch (int ret)
{
    return ret;
}
catch (exception const &exc)
{
    cout << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cout << "\n"
            "fatal: unaccounted for exception\n";
    return 1;
}
