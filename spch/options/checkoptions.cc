#define XERR
#include "options.ih"

void Options::checkOptions()
{
    if (
        not
        (d_listOption or d_precompileOption or d_softLinkOption)
    )
        emsg << "icm-spch: one of the options --all, --list, --precompile or "
                " --soft-link must be specified" << endl;
    else if (not d_all)
    {        
        if (
            (d_listOption and (d_precompileOption or d_softLinkOption))
            or
            (d_precompileOption and d_softLinkOption)
        )
            emsg << "only one of the options --list, --precompile or "
                    "--soft-link can be specified" << endl;
    }

    if (emsg.count() != 0)
        throw 1;
}
