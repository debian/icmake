#define XERR
#include "options.ih"

void Options::readUnusedFile(string const &filename)
{
    ifstream in{ Exception::factory<ifstream>(filename) };

    string regex;
    string line;
    while (getline(in, line))
    {
        line = String::trim(line);
        if (not line.empty())
            regex += '(' + line + ")|";
    }

    if (regex.empty())
    {
        emsg << "--unused filename: at least one regex is required" << endl;
        return;
    }

    regex.resize(regex.length() - 1);       // rm the final '|'
    s_unused = Pattern{ regex };
}
