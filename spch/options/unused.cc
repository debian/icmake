#define XERR
#include "options.ih"

// static
bool Options::unused(std::string const &spec)
{
    return s_unused << spec;
}
