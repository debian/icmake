#define XERR
#include "options.ih"

bool Options::allOpt()
{
    if (d_all = d_arg.option(&d_spch, 'a');
        not d_all
    )
        return false;

    disallowed("lps");
    setList("--all");
    setSpchGch();
    d_softLinkOption = true;

    return true;
}
