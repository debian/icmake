#define XERR
#include "options.ih"

void Options::unusedOpt()
{
    if (d_arg.option('k'))
        wmsg << "--keep (-k) is deprecated. Use --unused (-u)" << endl;

    string value;
    if (d_unusedOption = d_arg.option(&value, "ku"); not d_unusedOption)
        return;

    error_code ec;
    if (value.find("f:") == 0)
        readUnusedFile(value.substr(2));
    else
        s_unused = Pattern{ value };
}
