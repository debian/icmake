#define XERR
#include "options.ih"

    // option is --precompile, --soft-link
    // optChar is -p or -s
    // called from precompileOpt and softlinkOpt

bool Options::spchGch(char const *option, int optChar)
{
    if (not (d_arg.option(&d_spch, optChar) and plainSpchName(option)))
        return false;

    setSpchGch();
    return true;
}   
