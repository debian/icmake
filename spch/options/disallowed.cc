#define XERR
#include "options.ih"

void Options::disallowed(char const *options) const
{
    if (d_arg.option(options))
        throw Exception{} << "--all, --list, --precompile, and --soft-link "
                            " are mutually exclusive";
}
