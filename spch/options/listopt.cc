#define XERR
#include "options.ih"

void Options::listOpt()
{
    if (                                    // continue at --list option
        d_listOption = d_arg.option('l');
        d_listOption
    )
    {
        disallowed("ps");
        d_spch = d_arg[0];
        setList("--list");
    }
}
