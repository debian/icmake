#define XERR
#include "options.ih"

#include <sstream>

Options::Options()
:
    d_arg(Arg::instance()),
    d_quiet(d_arg.option('q'))
{
    if (d_arg.option('w'))
        wmsg << "option '--warn' is obsoleted and can be omitted" << endl;

    classesOpt();

    // -i:          d_arg[0] is spch
    // -p, -s -a:   d_arg[0] is spch, d_arg[1] is (e.g.) tmp/spch.gch
    // with -p and -s 'spch' is the option argument; 'tmp/spch.gch' is arg[0]
    // with -a 'spch' is arg[0] and 'tmp/spch.gch' is arg[1]

    internalOpt();

    ignored('g', "12.04.00");
    ignored('r', "12.04.00");

    unusedOpt();

    if (not allOpt())
    {
        listOpt();
        precompileOpt();
        softLinkOpt();
    }

//xerr("options/");

    topDirOpt();

    checkOptions();
}
