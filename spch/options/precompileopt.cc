#define XERR
#include "options.ih"

void Options::precompileOpt()
{
    d_precompileOption = spchGch("--precompile", 'p');

    if (not d_precompileOption)
        return;

    disallowed("s");
}
