#define XERR
#include "options.ih"

void Options::internalOpt()
{
    if (d_arg.option(&d_internal, 'i') and d_internal.front() != '.')
        emsg << "the --internal option value must start with a dot (.)" << 
                                                                        endl;
    d_internalOption = not d_internal.empty();

    if (not d_internalOption)
        d_internal = ".ih";
}
