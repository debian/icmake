inline bool Options::all() const
{
    return d_all;
}

inline std::string const &Options::extension() const
{
    return d_internal;
}

inline std::string const &Options::classes() const
{
    return d_classes;
}

inline std::string const &Options::internal() const
{
    return d_internal;
}

inline bool Options::list() const
{
    return d_listOption;
}

inline bool Options::precompile() const
{
    return d_precompileOption;
}

inline bool Options::quiet() const
{
    return d_quiet;
}

inline bool Options::softLink() const
{
    return d_softLinkOption;
}

inline bool Options::topDir() const
{
    return d_topDir;
}

inline std::string const &Options::spch() const
{
    return d_spch;
}

inline std::string const &Options::spchGch() const
{
    return d_spchGch;
}

inline char const *Options::compilerArg() const
{
    return d_arg[1 + d_all];
}

inline std::string const &Options::extraOptions() const
{
    return d_extraOptions;
}

