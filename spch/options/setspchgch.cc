#define XERR
#include "options.ih"

    // idx = 0 with -p and -s
    // idx = 1 with -a

void Options::setSpchGch()
{
    d_spchGch = d_arg[0];                  // name/path to spch.gch 

                // add all avail. -o option(s), each option starts with a ' '
                // (cf. process/precompile.cc)
    string value;
    for (size_t idx = 0, end = d_arg.option('o'); idx != end; ++idx)
    {
        d_arg.option(idx, &value, 'o');     // get the option value
        (d_extraOptions += ' ') += value;
    }

    if (d_spchGch.back() == '/')                // last == '/': append d_spch 
        d_spchGch += d_spch + ".gch";

    error_code ec;
    if (not Tools::createDirectories(fs::path{ d_spchGch }.parent_path()))
        throw Exception{} << "cannot create the directory of " << d_spchGch;
}




