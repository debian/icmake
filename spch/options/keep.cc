#define XERR
#include "options.ih"

// static
bool Options::keep(std::string const &spec)
{
    wmsg << "keep(...) is deprecated. Use unused()" << endl;
    return unused(spec);
}
