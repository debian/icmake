#ifndef INCLUDED_OPTIONS_
#define INCLUDED_OPTIONS_

#include <vector>
#include <string>
#include <iosfwd>

#ifdef fbb
    #include <bobcat/arg>
#else
    #include "../../tmp/build/arg/arg"
#endif

namespace FBB
{
    class Pattern;
}

class Options
{
    FBB::Arg const &d_arg;

    std::string d_spch;
    std::string d_spchGch;

    bool d_all;
    bool d_listOption;
    bool d_precompileOption;
    bool d_quiet;
    bool d_softLinkOption;
    bool d_topDirOption;
    bool d_topDir;

    std::string d_extraOptions;     // extra options used with --precompile

    bool d_internalOption;
    std::string d_internal;

    bool d_classesOption;
    std::string d_classes;

    bool d_unusedOption;
    static FBB::Pattern s_unused;
    static FBB::Pattern s_spec;     // filename spec. pattern for --unused

    public:
        Options();

        bool all() const;
        bool list() const;
        bool topDir() const;
        std::string const &classes() const;
        std::string const &spch() const;
        std::string const &spchGch() const;
        std::string const &extension() const;       // the targeted extension
        std::string const &extraOptions() const;
        char const *compilerArg() const;            // d_arg[1] with --precomp
                                                    // d_arg[2] with --all
        std::string const &internal() const;        // the .ih extension

        static bool keep(std::string const &spec);      // deprecated
        static bool unused(std::string const &spec);    // #include file spec.
        bool precompile() const;
        bool quiet() const;
        bool softLink() const;

    private:
        bool allOpt();
        bool plainSpchName(char const *option);
        bool spchGch(char const *option, int optChar); 
        void checkOptions();
        void classesOpt();
        void disallowed(char const *options) const;
        void ignored(int option, char const *version) const;
        void internalOpt();
        void listOpt();
        void precompileOpt();
        void readUnusedFile(std::string const &filename);
        void setList(char const *optName);
        void setSpchGch();
        void softLinkOpt();
        void topDirOpt();
        void unusedOpt();
};

#include "options.f"
        
#endif
