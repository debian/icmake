#define XERR
#include "options.ih"

void Options::ignored(int option, char const *version) const
{
    if (d_arg.option(option))
        wmsg << "the --guard option is ignored starting from "
                "version " << version << endl;
}
