#define XERR
#include "process.ih"

    // returns 0 if OK
int Process::spch() const
{                                   // currently rh() is not used anymore

                                // read the .ih files specifications fm 
                                // CLASSES, write them to the spch file.
    ofstream out = Exception::factory<ofstream>( d_options.spch() );

    for (string const &line: StringSet{ Classes{ d_options }.readClasses() })
    {
        if (Tools::exists(line))
            out << "#include \"" + line + "\"\n";
    }

    return 0;
}
