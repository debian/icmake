#define XERR
#include "process.ih"

    // returns 0 if already or precompilation OK
int Process::precompile()
{
    string const &spch = d_options.spch();
    string const &spchGch = d_options.spchGch();

    bool ok = recent(spchGch, spch);
    if (not ok)
    {
        string compilerSpec = 
                    Tools::compilerSpec(d_options.compilerArg(), 
                                        s_precompileCmd);

        if (size_t pos = compilerSpec.find(" -x"); pos != string::npos)
            compilerSpec = compilerSpec.substr(0, pos) +
                            d_options.extraOptions() + 
                            compilerSpec.substr(pos);

        string cmd{ Tools::command(compilerSpec, spch, spchGch) };
        if (not d_options.quiet())
            cout << cmd << '\n';

        Exec exec;
        exec.execute(cmd);
        ok =  exec.ret() == 0;
    }

    if (ok)
    {
        cout << spchGch << ": ";
        double size = Stat{ spchGch }.size();
        cout << setiosflags(ios::fixed) << setprecision(1) <<
            (size / (1024 * 1024)) << " MB\n";
    }

    return ok == true ? 0 : 1;
}
