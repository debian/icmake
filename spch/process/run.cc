#define XERR
#include "process.ih"

int Process::run()
{
    if (d_options.all())
        return 
            spch() != 0       ? 1 :
            precompile() != 0 ? 2 :
            softLinks() != 0  ? 3 : 0;

    if (d_options.list())           // construct the ./spch file
        return spch();

    if (d_options.precompile())     // precompile the header at 'p' to dest
        return precompile();

    if (d_options.softLink())       // construct the soft-links to dest.
        return softLinks();

    throw Exception{} << "at least one of --list, --precompile or "
                         "--soft-links must be specified";
}
