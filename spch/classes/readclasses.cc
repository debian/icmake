#define XERR
#include "classes.ih"

Classes::StringSet const &Classes::readClasses()
{
    string const &classes = d_options.classes();
    string const &extension = d_options.extension();

    if (not Tools::exists(classes))
        wmsg << '`' << classes << "' does not exist" << endl;
    else
    {
        ifstream in{ Exception::factory<ifstream>(classes) };
    
        string line;
        while (getline(in, line))
        {                                       // rm eoln comment
            if (size_t pos = line.find("//"); pos != string::npos)
                line.resize(pos);
                                                // rm #-comment
            if (size_t pos = line.find('#'); pos != string::npos)
                line.resize(pos);

            string trimmed = String::trim(line);    // accept non-empty lines
            if (not trimmed.empty())
                add(trimmed, extension);
        }
    }

    if (d_options.topDir())
        add(extension);
    
    return d_files;
}
