//                     usage.cc

#include "main.ih"

namespace {
char const info[] = R"_( [options] dest ['compiler']
Where:
    [options] - optional arguments (short options between parentheses):
        --all (-a) file        - call in sequence: icmake -S's options --list,
                                 --precompile), and --soft-links, where file
                                 is written by option --list and used as
                                 option argument for --precompile and
                                 --soft-links.
        --classes (-c) file    - 'file' contains the list of directories
                                 inspected by the --list option (by default
                                 CLASSES). The project's top directory is
                                 automatically inspected unless the option
                                 --no-topdir is specified.
        --help (-h)            - provide this help and end the program.
        --internal (-i) .ext   - .ext is the extension used for the internal
                                 headers (including the dot) by default: .ih
        --list (-l)            - ignored when --all is specified.
                                 Write the names of the files to process when
                                 constructing an SPCH to the file specified as
                                 icmake -S's first command line argument. The
                                 specified filename may not have an extension
                                 or directory specifications (e.g., spch).
        --no-topdir (-n)       - ignore the project's top directory when
                                 specifying the --list option.
        --options (-o) options - extra options added to the compiler's options
                                 when --precompile is requested. The 'options'
                                 are inserted before the compiler's option
                                 '-x'. Multiple --options options can be
                                 specified. 
        --precompile (-p) file - ignored when --all is specified.  
                                 Precompiles 'file' (the filename specified at
                                 the option '--list') to the SPCH file
                                 specified as icm-spch's first command-line
                                 argument. If that argument ends in / then the
                                 SPCH file 'argument'file.gch is written.
                                By default the SPCH is constructed using the
                                 following command:
             g++ -c -o $2 ${ICMAKE_CPPSTD} -Wall -Werror -O2 -x c++header $2'
                                Here, $1 refers to 'file', $2 refers to
                                 'dest', and '$ICMAKE_CPPSTD' refers to the
                                 value of the 'ICMAKE_CPPSTD' environment
                                 variable (specifying the bf(C++) standard to
                                 use.
                                Alternatively, the command writing the SPCH
                                 can be provided as second command-line
                                 argument (in which case it should be quoted),
                                 or the second command-line argument can be
                                 'f:file', where 'file' is the name of a file
                                 whose first line specifies the command
                                 constructing the SPCH (which must specify
                                 '$1' and '$2' and optionally
                                 '$ICMAKE_CPPSTD').nl()
                                The 'PATH' environment variable is used to
                                 locate the compiler, but the compiler's
                                 absolute path can also be used.
        --quiet (-q)           - the command executed by --precompile is not 
                                 echoed to the standard output stream.
        --soft-link (-s) file  - ignored when --all is specified.
                                 'file' and 'dest' are identical to the ones
                                 specified at option --precompile. --soft-link
                                 creates .gch soft-links from the header files
                                 listed in 'file' to the SPCH-file written by
                                 --precompile. 
        --unsed (-u) regex     - In practice this option is probably not
                                 required, but files matching (POSIX extended)
                                 regex are not inspected by 'icmake -S'. When
                                 used specifications like (...)|(...) can be
                                 used to specify multiple
                                 regexes. Alternatively f:file can be used as
                                 option argument to specify a file whose
                                 non-empty lines contain regexes;
        --version (-v)         - show version information and end the program.

    Only one of the options --all, --list, precompile, or --soft-links can be
    specified 

)_";

}

void usage(std::string const &progname)
{
    Tools::usageTop(progname) << info;
}
